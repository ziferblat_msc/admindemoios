//
//  ZFAlarmClockViewController.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFAlarmClockViewController.h"
#import "ZFDeleteAlarmClock.h"
#import "ZFGuestCell.h"
#import "LOCheckOutDialog.h"
#import "LOVenue.h"
#import "ZFGetAlarmClockRequest.h"

@implementation ZFAlarmClockViewController
{
    UIButton *addButton;
    UIButton *deleteButton;
    
    LOCheckOutDialog *checkOutDialog;
}

static NSString *guestCellIdentifier = @"GuestCell";

- (id)initWithMember:(LOMember *)aMember alarmClock:(ZFAlarmClock *)anAlarmClock
{
    self = [super initWithMember:aMember];
    
    if(self != nil)
    {
        alarmClock = anAlarmClock;
        venue = anAlarmClock.venue;
        self.title = alarmClock.name;
    }
    
    return self;
}

- (void)loadView
{
    [super loadView];
    [super showBack];
    [self setupViews];
}

- (UIView *)headerContent
{    
    UIView *container = [[UIView alloc] init];
    
    addButton = [super addButton:@"Add Guest"];
    addButton.width = 120.0;
    [container addSubview:addButton];
    addButton.top += 10;
    [addButton addTarget:self action:@selector(addPressed) forControlEvents:UIControlEventTouchUpInside];
    [container setSize:CGSizeMake(addButton.right, addButton.height)];
    
    deleteButton = [super addButton:@"Delete"];
    [deleteButton setBackgroundColor:Colour_Red forState:UIControlStateNormal];
    deleteButton.width = 120.0;
    [container addSubview:deleteButton];
    [deleteButton centerHorizontallyInSuperView];
    deleteButton.top += 10;
    deleteButton.left = addButton.right + 90.0;;
    [deleteButton addTarget:self action:@selector(deletePressed) forControlEvents:UIControlEventTouchUpInside];
    [container setSize:CGSizeMake(deleteButton.right, deleteButton.height)];
    
    deleteButton.hidden = alarmClock.members.count > 0 ? TRUE : FALSE;
    
    if(deleteButton.hidden)
    {
        [addButton centerHorizontallyInSuperView];
    }
    else
    {
        addButton.left = 0;
    }
    
    return container;
}

- (void)setupViews
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(120, 142)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:50.0];
    [flowLayout setMinimumLineSpacing:60.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(30.0, 50.0, 30.0, 50.0)];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, contentView.width, contentView.height) collectionViewLayout:flowLayout];
    [self.collectionView setBackgroundColor:Colour_Clear];
    [self.collectionView registerClass:[ZFGuestCell class] forCellWithReuseIdentifier:guestCellIdentifier];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [contentView addSubview:self.collectionView];
    
    self.noDataLabel = [[UILabel alloc] init];
    [self.noDataLabel setFont:Font(18.0)];
    [self.noDataLabel setTextColor:Colour_Gray(0.5)];
    [contentView addSubview:self.noDataLabel];
    self.noDataLabel.text = NSLocalizedString(@"No Guests", nil);
    [self.noDataLabel sizeToFit];
    [self.noDataLabel centerInSuperView];
}

- (void)addPressed
{
    LOMember *newGuest = [[LOMember alloc] init];
    
    ZFNewGuestDialogView * dialog = [[ZFNewGuestDialogView alloc] initWithGuest:newGuest alarmClockId:alarmClock.id];
    dialog.delegate = self;
    
    [self.navigationController.tabBarController.view addSubview:dialog];
    [dialog show];
}

- (void)deletePressed
{
    [UIAlertView showWithTitle:@"Delete Alarm Clock" message:@"Are you sure you wish to delete this Alarm Clock?" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
    {
        if(buttonIndex == 1)
        {
            [self deleteAlarmClock:alarmClock];
        }
    }];
}

- (void)deleteAlarmClock:(ZFAlarmClock *)anAlarmClock
{
    Show_Hud;
    
    [ZFDeleteAlarmClock requestWithAlarmClockId:anAlarmClock.id completion:^(LORESTRequest *aRequest)
    {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed)
        {
            //TODO
            return;
        }
        
        [self.navigationController popViewControllerAnimated:TRUE];
    }];
}

#pragma mark -
#pragma mark Collection View Datasource & Delegate Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.collectionView.alpha = alarmClock.members.count > 0 ? 1.0 : 0.0;
    self.noDataLabel.alpha = alarmClock.members.count > 0 ? 0.0 : 1.0;
    
    deleteButton.hidden = alarmClock.members.count > 0 ? TRUE : FALSE;
    
    if(deleteButton.hidden)
    {
        [addButton centerHorizontallyInSuperView];
    }
    else
    {
        addButton.left = 0;
    }
    
    return alarmClock.members.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZFGuestCell *cell = (ZFGuestCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:guestCellIdentifier                                                                                                         forIndexPath:indexPath];
    
    NSDictionary *guest = [alarmClock.members objectAtIndex:indexPath.row];
    [cell setGuest:guest];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *guest = [alarmClock.members objectAtIndex:indexPath.row];
    
    NSString *guestName = [guest objectForKey:@"firstName"];
    NSInteger guestId = [[guest objectForKey:@"id"] intValue];
    
    [UIAlertView showWithTitle:guestName message: [NSString stringWithFormat:@"Would you like to check-out guest %@?", guestName ] cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Check-out"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
     {
         if(buttonIndex == 1)
         {
             LOMember *guest = [[LOMember alloc] init];
             guest.memberId = guestId;
             guest.currentVenueId = alarmClock.venue.venueId;
             
             [self checkOutMember:guest];
         }
     }];
}

#pragma mark -
#pragma mark Requests

- (void)checkOutMember:(LOMember *)aMember
{
    Show_Hud;
    
    [[LOMemberService instance] checkOutMember:aMember completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
            Show_Error_Deprecated(@"There was an error checking-out this member. Please try again.");
            return;
        }
        
        checkOutDialog = [[LOCheckOutDialog alloc] initWithMember:aMember checkOut:aRequest.result];
        [checkOutDialog setDelegate:self];
        [self.navigationController.tabBarController.view addSubview:checkOutDialog];
        [checkOutDialog show];
    }];
}

- (void)confirmCheckOutMember:(LOMember *)checkOutMember checkOut:(LOCheckOut *)aCheckOut
{
    Show_Hud;
    [[LOMemberService instance] confirmCheckOutMember:checkOutMember
                                             checkOut:aCheckOut
                                           completion:^(LORESTRequest *aRequest)
    {
        Hide_Hud;
        if(aRequest.resultStatus != LORESTRequestResultStatusSucceeded)
        {
             Show_Error_Deprecated(@"There was an error checking-out this member. Please try again.");
             return;
        }
        
        [self getAlarmCLock];
        
        [checkOutDialog dismiss:nil];
    }];
    
}

- (void)getAlarmCLock
{
    Show_Hud;
    
    [ZFGetAlarmClockRequest requestWithAlarmClockId:alarmClock.id completion:^(LORESTRequest *aRequest)
     {
         Hide_Hud;
         if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded)
         {
             alarmClock = aRequest.result;
             alarmClock.venue = venue;
             
             [self.collectionView reloadData];
         }
     }];
}

#pragma mark -
#pragma mark ZFNewGuestDialogViewDelegate Methods

- (void)alarmClockChanged:(ZFAlarmClock *)anAlarmClock
{
    alarmClock = anAlarmClock;
    alarmClock.venue = venue;
    
    [self.collectionView reloadData];
}

#pragma mark -
#pragma <LOCheckOutRestrictionsDelegate>

- (void)confirmCheckOutPressed:(LOCheckOut *)aCheckOut forMember:(LOMember *)aMember
{
    [self confirmCheckOutMember:aMember checkOut:aCheckOut];
}

@end
