//
//  LODashboardViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOTabBarController.h"

@class LOMember;

@interface LODashboardViewController : LOTabBarController {
    UIView *contentView;
    LOMember *member;
    NSArray *venues;
}

- (id)initWithMember:(LOMember *)aMember;

@end
