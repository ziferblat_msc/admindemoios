//
//  LOTabBarController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LOTabBarController : UITabBarController

@end
