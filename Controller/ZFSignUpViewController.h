//
//  ZFSignUpViewController.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 18.04.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LOBaseViewController.h"

@class LOBlockButton;

@interface ZFSignUpViewController : LOBaseViewController <UITextFieldDelegate>
{
    UITextField *emailTextField;
    UITextField *secretWordTextField;
    UITextField *passwordTextField;
    UITextField *confirmPasswordTextField;
    LOBlockButton *backButton;
}

@end
