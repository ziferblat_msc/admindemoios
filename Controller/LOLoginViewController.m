//
//  LOLoginViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOLoginViewController.h"
#import "LODashboardViewController.h"
#import "LOForgotPasswordViewController.h"
#import "ZFSignUpViewController.h"
#import "LOMemberSelectorView.h"
#import "LOBlockButton.h"
#import "LOMember.h"
#import "LORESTRequestService.h"

@implementation LOLoginViewController

#define kLoginScrollHeight  200.0
#define kSelectorPadding    100.0
#define kSelectorSize       120.0
#define kSelectorOffset     50.0
#define kPageOffset         300.0
#define kSelectorLeft       ((self.view.width - ((kSelectorSize * MIN(3, members.count)) + (kSelectorPadding * (MIN(3, members.count) - 1)))) * 0.5)

- (void)loadView
{
    [super loadView];
    
    emailField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [emailField setEnablesReturnKeyAutomatically:TRUE];
    [emailField setPlaceholder:@"Email"];
    [emailField setTextAlignment:NSTextAlignmentCenter];
    [emailField setBorderStyle:UITextBorderStyleRoundedRect];
    [emailField setDelegate:self];
    [self.view  addSubview:emailField];
    [emailField centerInSuperView];
    
    passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [passwordField setEnablesReturnKeyAutomatically:TRUE];
    [passwordField setPlaceholder:@"Password"];
    [passwordField setTextAlignment:NSTextAlignmentCenter];
    [passwordField setBorderStyle:UITextBorderStyleRoundedRect];
    [passwordField setSecureTextEntry:TRUE];
    [passwordField setDelegate:self];
    [self.view addSubview:passwordField];
    [passwordField centerHorizontallyInSuperView];
    passwordField.top = emailField.bottom + kPadding;
    passwordField.returnKeyType = UIReturnKeyGo;
    
    backButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [backButton setSize:CGSizeMake(44.0, 44.0)];
    [backButton setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
    //[self.view addSubview:backButton];
    backButton.alpha = 0.0;
    
    forgotPasswordButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [forgotPasswordButton setSize:CGSizeMake(150, 30)];
    [forgotPasswordButton setTitle:@"Forgot Password" forState:UIControlStateNormal];
    [forgotPasswordButton.titleLabel setFont:Font(14.0)];
    [self.view addSubview:forgotPasswordButton];
    
    [forgotPasswordButton handleControlEvent:UIControlEventTouchUpInside withBlock:^
     {
         [self goToForgotPassword];
     }];
    
    forgotPasswordButton.top = passwordField.bottom + kPadding;
    [forgotPasswordButton centerHorizontallyInSuperView];
    
    signupButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [signupButton setSize:CGSizeMake(300, 30)];
    [signupButton setTitle:@"Become a new Admin member" forState:UIControlStateNormal];
    [signupButton.titleLabel setFont:Font(20.0)];
    [self.view addSubview:signupButton];
    
    [signupButton handleControlEvent:UIControlEventTouchUpInside withBlock:^
     {
         [self goToSignUp];
     }];
    
    signupButton.top = forgotPasswordButton.bottom + kPadding*6;
    [signupButton centerHorizontallyInSuperView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setFont:Font(24)];
    [titleLabel setTextColor:Colour_LightBlue];
    [titleLabel setBackgroundColor:Colour_Clear];
    [titleLabel setText:@"Login for Admins"];
    [titleLabel sizeToFit];
    [self.view addSubview:titleLabel];
    [titleLabel centerHorizontallyInSuperView];
    titleLabel.bottom = emailField.top - (kLargePadding);
    
    UILabel *orLabel = [[UILabel alloc] init];
    [orLabel setFont:Font(16)];
    [orLabel setTextColor:Colour_LightBlue];
    [orLabel setBackgroundColor:Colour_Clear];
    [orLabel setText:@"- or -"];
    [orLabel sizeToFit];
    [self.view addSubview:orLabel];
    [orLabel centerHorizontallyInSuperView];
    orLabel.top = forgotPasswordButton.bottom + kPadding*2;
    
    UILabel *helpLabel = [[UILabel alloc] init];
    [helpLabel setFont:Font_Italic(14.0)];
    [helpLabel setTextColor:Colour_LightBlue];
    [helpLabel setBackgroundColor:Colour_Clear];
    [helpLabel setText:@"Can’t login? Contact a Global Team Member"];
    [helpLabel sizeToFit];
    [self.view addSubview:helpLabel];
    [helpLabel centerHorizontallyInSuperView];
    helpLabel.bottom = self.view.height - (kLargePadding * 2.0);
}

-(void)viewDidLoad
{
    LOMember *previousMember = UserService.previousMember;
    
    if(previousMember)
    {
        emailField.text = previousMember.email;
        
        if (previousMember.password)
        {
            [self login:previousMember.email password:previousMember.password];
        }
    }
}

#pragma mark - Navigation

- (void)goToForgotPassword
{
    [self.view endEditing:YES];
    LOForgotPasswordViewController *controller = [[LOForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:controller animated:TRUE];
}

- (void)goToSignUp
{
    [self.view endEditing:YES];
    ZFSignUpViewController *controller = [[ZFSignUpViewController alloc] init];
    [self.navigationController pushViewController:controller animated:TRUE];
}

#pragma mark -
#pragma mark Request Methods

- (void)login:(NSString *)aUsername password:(NSString *)aPassword {
    [[LOMemberService instance] login:aUsername password:aPassword completion:^(LORESTRequest *aRequest)
    {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded)
        {
            LOMember *member = aRequest.result;
            member.password = aPassword;
            
            [[LORESTRequestService instance] setUsername:aUsername];
            [[LORESTRequestService instance] setPassword:aPassword];
            
            [UserService setLoggedInMember:member];
            
            LODashboardViewController *controller = [[LODashboardViewController alloc] initWithMember:member];
            [self.navigationController pushViewController:controller animated:TRUE];
            
            passwordField.text = @"";
        }
        else
        {
            [passwordField setText:nil];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ziferblat" message:@"Invalid password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == emailField)
    {
        [passwordField becomeFirstResponder];
        return NO;
    }
    else if(textField == passwordField)
    {
        [textField resignFirstResponder];
        [self login:emailField.text password:passwordField.text];
        return NO;
    }
    
    // Disable back button to prevent the bug ZFAD-51
    backButton.enabled = NO;
    
    return YES;
}

#pragma mark -
#pragma mark Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];

    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.top = -150.0;
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    
    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.top = 0.0;
    } completion:nil];
}

@end
