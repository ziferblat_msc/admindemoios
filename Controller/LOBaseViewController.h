//
//  LOBaseViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LOBaseViewController : UIViewController {
    UIViewAnimationOptions curve;    
    CGFloat duration;
    CGSize keyboardSize;
    UIView *currentItem;
}

@end
