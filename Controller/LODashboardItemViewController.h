//
//  LODashboardItemViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOBaseViewController.h"
#import "LOMemberInfoView.h"

@class LOMember;

@interface LODashboardItemViewController : LOBaseViewController <LOMemberInfoViewDelegate>{
    UIView *headerView;
    UIView *contentView;
    LOMember *member;
    UILabel *titleLabel;
    UIView *divider;
    UIView *headerContent;
    UIView *headerBottomContent;
}

- (id)initWithMember:(LOMember *)aMember;
- (UIView *)headerContent;
- (UIView *)headerBottomContent;
- (void)updateTitle;
- (UIButton *)addButton:(NSString *)aCaption;
- (void)showBack;
- (void)showAdd:(NSString *)aCaption;
- (void)showHeaderContent:(BOOL)animated;
- (void)hideHeaderContent:(BOOL)animated;

@end
