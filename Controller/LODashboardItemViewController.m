//
//  LODashboardItemViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"
#import "LOMember.h"
#import "LOMemberInfoView.h"
#import "LOEditPasswordView.h"

@implementation LODashboardItemViewController

#define kButtonHeight           44.0

- (id)initWithMember:(LOMember *)aMember {
    self = [super init];
    
    if(self != nil) {
        member = aMember;
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadView {
    [super loadView];
    
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 150)];
    [self.view addSubview:headerView];
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0)];
    [headerView addSubview:container];
    
    titleLabel = [[UILabel alloc] init];
    [titleLabel setBackgroundColor:Colour_Clear];
    [titleLabel setTextColor:Colour_White];
    [titleLabel setFont:Font_Bold(18.0)];
    [container addSubview:titleLabel];
    [self updateTitle];
    
    divider = [UIView dividerWithWidth:200 colour:Colour_WhiteAlpha(0.5)];
    [container addSubview:divider];
    [divider centerHorizontallyInSuperView];
    divider.top = titleLabel.bottom + kPadding;

    UIView *headerContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 40.0)];
    headerContent = [self headerContent];
    [headerContainer addSubview:headerContent];
    [headerContent centerInSuperView];
    [container addSubview:headerContainer];
    headerContainer.top = divider.bottom + kPadding;
    
    UIView *headerBottom = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 40)];
    headerBottomContent = [self headerBottomContent];
    [headerBottom addSubview:headerBottomContent];
    [headerBottomContent centerInSuperView];
    [container addSubview:headerBottom];
    headerBottom.top = headerContainer.bottom + kSmallPadding;
    
    container.height = headerBottom.bottom;
    [container centerInSuperView];
    headerView.top = 20;
    
    CGFloat tabHeight = self.tabBarController.tabBar.height;
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - (headerView.bottom + tabHeight))];
    [contentView setBackgroundColor:Colour_White];
    [self.view addSubview:contentView];
    contentView.bottom = self.view.height - tabHeight;
    
    LOMemberInfoView *infoView = [[LOMemberInfoView alloc] init];
    infoView.delegate = self;
    [headerView addSubview:infoView];
    [infoView centerVerticallyInSuperView];
    infoView.right = self.view.width - kLargePadding;
    [infoView addTarget:self action:@selector(didLogout) forControlEvents:LOMemberInfoViewDidLogout];
    [infoView addTarget:self action:@selector(didCheckIn) forControlEvents:LOMemberInfoViewDidCheckIn];
    [infoView addTarget:self action:@selector(didCheckOut) forControlEvents:LOMemberInfoViewDidCheckOut];
    
}

- (UIButton *)addButton:(NSString *)aCaption {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:Colour_LightGray];
    [button setTitle:aCaption forState:UIControlStateNormal];
    [button.titleLabel setFont:Font(14.0)];
    [button.layer setCornerRadius:10];
    [button sizeToFit];
    [button setClipsToBounds:TRUE];
    button.width += 40.0;
    button.height = kButtonHeight;
    return button;
}

- (void)updateTitle {
    [titleLabel setText:self.title];
    [titleLabel sizeToFit];
    [titleLabel centerHorizontallyInSuperView];
}

- (UIView *)headerContent {
    return nil;
}

- (UIView *)headerBottomContent {
    return nil;
}

- (void)showHeaderContent:(BOOL)animated {
    [self updateTitle];
    
    [UIView animateWithDuration:animated ? 0.3 : 0.0 animations:^{
        titleLabel.top = 0.0;
        divider.alpha = 1.0;
        headerContent.alpha = 1.0;
    }];
}
- (void)hideHeaderContent:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.3 : 0.0 animations:^{
        [titleLabel centerVerticallyInSuperView];
        divider.alpha = 0.0;
        headerContent.alpha = 0.0;
    }];
}

- (UIButton *)addLeftButton:(NSString *)aCaption image:(NSString *)anImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:aCaption forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:anImage] forState:UIControlStateNormal];
    [button sizeToFit];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, -5)];
    button.width += 5.0;
    [headerView addSubview:button];
    [button centerVerticallyInSuperView];
    button.left = kLargePadding;
    return button;
}

- (void)showBack {
    UIButton *backButton = [self addLeftButton:@"Back" image:@"BackChevronIcon"];
    [backButton addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)showAdd:(NSString *)aCaption {
    UIButton *addButton = [self addLeftButton:aCaption image:@"NewItemIcon"];
    [addButton addTarget:self action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
}

- (void)backPressed {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)addItem { }

- (void)didLogout
{
    [UserService logout];
    [LORESTRequestService logout];
    [self.tabBarController.navigationController popToRootViewControllerAnimated:TRUE];
}

#pragma mark -
#pragma mark Request Methods

- (void)didCheckOut {
    [[LOMemberService instance] checkOutMember:member completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            return;
        }
        LOCheckOut *checkout = (LOCheckOut *)aRequest.result;
        
        [[LOMemberService instance] confirmCheckOutMember:member checkOut:checkout completion:^(LORESTRequest *aRequest) {
            if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                LOMember *resultMember = aRequest.result;
                [LoggedInMember setCurrentVenueId:resultMember.currentVenueId];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserDidChange object:nil];
            } else {
                [UIAlertView showWithTitle:@"Error"
                                   message:[NSString stringWithFormat:@"Failed to check out"]
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserDidFail object:nil];
                                  }];
            }
        }];
    }];
}

- (void)didCheckIn {
    [[LOMemberService instance] checkInMember:member completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            return;
        }
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            LOMember *resultMember = aRequest.result;
            [LoggedInMember setCurrentVenueId:resultMember.currentVenueId];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserDidChange object:nil];
        } else {
            [UIAlertView showWithTitle:@"Error"
                               message:[NSString stringWithFormat:@"Failed to check in"]
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserDidFail object:nil];
                              }];
        }
        
    }];
}

#pragma mark -
#pragma mark <LOMemberInfoViewDelegate>

- (void)presentChangePasswordWindowFor:(LOMember *)aMember {
    LOEditPasswordView *editPasswordView = [[LOEditPasswordView alloc] initWithMember:aMember];
    [self.view addSubview:editPasswordView];
    [editPasswordView show];
}

@end
