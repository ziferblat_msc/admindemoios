//
//  LOMembersViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOMembersViewController.h"
#import "LOMemberHeaderCell.h"
#import "LOMemberCell.h"
#import "LOMember.h"
#import "LOAccountBalanceRequest.h"
#import "LOAccountBalance.h"
#import "LOVenueService.h"
#import "LOVenue.h"
#import "LOCheckOutRestrictionsModel.h"
#import "LODebt.h"
#import "LOMemberService.h"
#import "LOCheckOut.h"
#import "LOGuestService.h"

typedef NS_ENUM(NSInteger, MemberFilterType) {
    MemberFilterTypeAll,
    MemberFilterTypeCheckedIn,
    MemberFilterTypeFavourite
};

@interface LOMembersViewController ()

@property (nonatomic, copy) NSString *sortText;
@property (nonatomic) MemberFilterType memberFilterType;
@property (nonatomic, strong) UISegmentedControl *filterSegmentedControl;

@end

@implementation LOMembersViewController

static NSString *memberCellIdentifier = @"MemberCell";
static NSString *memberHeaderIdentifier = @"MemberHeader";

- (id)initWithMember:(LOMember *)aMember {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        self.title = @"Members";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:self.title image:[UIImage imageNamed:@"MembersTabIcon"] selectedImage:nil];
        self.sortText = @"";
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(80, 100)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:50.0];
    [flowLayout setMinimumLineSpacing:50.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(30.0, 100.0, 30.0, 100.0)];
    [flowLayout setHeaderReferenceSize:CGSizeMake(kWidth, 30.0)];
    
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, contentView.width, contentView.height) collectionViewLayout:flowLayout];
    [collectionView setBackgroundColor:Colour_White];
    [collectionView registerClass:[LOMemberCell class] forCellWithReuseIdentifier:memberCellIdentifier];
    [collectionView registerClass:[LOMemberHeaderCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:memberHeaderIdentifier];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [contentView addSubview:collectionView];
    
    [super showAdd:[self newItemCaption]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.filterSegmentedControl setEnabled:[LOVenueService instance].isAssignedToVenue forSegmentAtIndex:1];
    [self.filterSegmentedControl setEnabled:[LOVenueService instance].isAssignedToVenue forSegmentAtIndex:2];
    if (![LOVenueService instance].isAssignedToVenue) {
        [self.filterSegmentedControl setSelectedSegmentIndex:0];
        self.memberFilterType = MemberFilterTypeAll;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self start];
}

- (void)start {
    [LONetworkBar showFromView:headerView];
    [self listMembers];
}

- (NSString *)newItemCaption {
    return @"New member";
}

- (UIView *)headerContent {
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 300, 60)];
    [searchBar setPlaceholder:@"Search members"];
    [searchBar setBackgroundImage:[UIImage new]];
    [searchBar setDelegate:self];
    return searchBar;
}

- (UIView *)headerBottomContent {
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    container.clipsToBounds = YES;
    
    self.filterSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[ NSLocalizedString(@"All", nil),
                                                                                           NSLocalizedString(@"Checked In", nil),
                                                                                           NSLocalizedString(@"Favourite", nil) ]];
    self.filterSegmentedControl.tintColor = [UIColor whiteColor];
    [container addSubview:self.filterSegmentedControl];
    [self.filterSegmentedControl centerVerticallyInSuperView];
    self.filterSegmentedControl.selectedSegmentIndex = 0;
    [self.filterSegmentedControl addTarget:self action:@selector(didChangeSegmentIndex:) forControlEvents:UIControlEventValueChanged];
    self.memberFilterType = MemberFilterTypeAll;
    
    UILabel *filterLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    filterLabel.text = @"Filter by: ";
    filterLabel.font = Font(14.0);
    filterLabel.textColor = [UIColor whiteColor];
    [filterLabel sizeToFit];
    [container addSubview:filterLabel];
    [filterLabel centerVerticallyInSuperView];
    
    container.width = ((filterLabel.width + kPadding) * 2) + self.filterSegmentedControl.width;
    [self.filterSegmentedControl centerHorizontallyInSuperView];
    filterLabel.right = self.filterSegmentedControl.left - kPadding;
    
    return container;
}

- (void)sortData:(NSArray *)data {
    NSString *currentPrefix;
    members = [[NSMutableArray alloc] init];
    NSArray *filteredMembersList = [self orderMembers:data];
    
    switch (self.memberFilterType) {
        case MemberFilterTypeCheckedIn: {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isCheckedIn == YES"];
            filteredMembersList = [filteredMembersList filteredArrayUsingPredicate:predicate];
            break;
        }
        case MemberFilterTypeFavourite: {
            if ([LOVenueService instance].assignedVenueId != 0) {
                // Only filter if device is assigned as members who have no favourite venue will have favouriteVenueId = 0
                // which will match the predicate
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favouriteVenueId == %ld", [LOVenueService instance].assignedVenueId];
                filteredMembersList = [filteredMembersList filteredArrayUsingPredicate:predicate];
            }
            break;
        }
        default: {
            break;
        }
    }
    
    if (![self.sortText isEqualToString:@""]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName CONTAINS[cd] %@ || lastName CONTAINS[cd] %@", self.sortText, self.sortText];
        filteredMembersList = [filteredMembersList filteredArrayUsingPredicate:predicate];
    }
    
    for (LOMember *aMember in filteredMembersList) {
        NSString *firstLetter = [self letterForMember:aMember];
        
        if ([currentPrefix isEqualToString:firstLetter]) {
            [[members lastObject] addObject:aMember];
        } else {
            NSMutableArray *newArray = [[NSMutableArray alloc] initWithObjects:aMember, nil];
            [(NSMutableArray *)members addObject:newArray];
        }
        
        currentPrefix = firstLetter;
    }
    
    [collectionView reloadData];
}

- (NSArray *)orderMembers:(NSArray *)data {
    NSArray *orderedArray = [data sortedArrayUsingComparator:^NSComparisonResult(LOMember * _Nonnull member1, LOMember * _Nonnull member2) {
        NSComparisonResult comparisonResult = [[member1.firstName uppercaseString] compare:[member2.firstName uppercaseString]];
        if (comparisonResult != NSOrderedSame) {
            return comparisonResult;
        } else {
            return [[member1.lastName uppercaseString] compare:[member2.lastName uppercaseString]];
        }
    }];
    return orderedArray;
}

- (void)showMember:(LOMember *)aMember {
    LOMemberDetailView *memberDetailView = [[LOMemberDetailView alloc] initWithMember:aMember];
    [memberDetailView setDelegate:self];
    [self.navigationController.tabBarController.view addSubview:memberDetailView];
    [memberDetailView show];
}

- (void)addItem {
    LOMember *newMember = [[LOMember alloc] init];
    LOCreateMemberView *editor = [[LOCreateMemberView alloc] initWithMember:newMember];
    [editor setDelegate:self];
    [self.navigationController.tabBarController.view addSubview:editor];
    [editor show];
}

- (NSString *)letterForMember:(LOMember *)aMember {
    if(aMember.firstName.length > 0) {
        return [[aMember.firstName substringToIndex:1] uppercaseString];
    } else {
        return @"!";
    }
}

#pragma mark -
#pragma mark Collection View Datasource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return members.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return ((NSArray *)[members objectAtIndex:section]).count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)aCollectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    NSArray *data = [members objectAtIndex:indexPath.section];
    LOMember *aMember = [data objectAtIndex:indexPath.row];
    NSString *firstLetter = [self letterForMember:aMember];
    
    LOMemberHeaderCell *header = (LOMemberHeaderCell *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:memberHeaderIdentifier forIndexPath:indexPath];
    [header setLetter:firstLetter];
    return header;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LOMemberCell *cell = (LOMemberCell *)[collectionView dequeueReusableCellWithReuseIdentifier:memberCellIdentifier                                                                                                         forIndexPath:indexPath];
    
    LOMember *aMember = [(NSArray *)[members objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell setMember:aMember];
    
    return cell;
}

#pragma mark -
#pragma mark Collection View Delegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LOMember *aMember = [[members objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self showMember:aMember];
}

#pragma mark -
#pragma mark Search Bar Delegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.sortText = searchText;
    [self sortData:allMembers];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISegmentedControl

- (void)didChangeSegmentIndex:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    if (segmentedControl.selectedSegmentIndex == 0) {
        // Filter all
        self.memberFilterType = MemberFilterTypeAll;
    } else if (segmentedControl.selectedSegmentIndex == 1){
        // Filter checked in
        self.memberFilterType = MemberFilterTypeCheckedIn;
    } else if (segmentedControl.selectedSegmentIndex == 2) {
        self.memberFilterType = MemberFilterTypeFavourite;
    } else {
        NSAssert(NO, @"Segment no implemented");
    }
    [self sortData:allMembers];
}

#pragma mark -
#pragma mark Request Method

- (void)listMembers {
    [[LOMemberService instance] listMembersWithCompletion:^(LORESTRequest *aRequest) {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        // Guest user needs to be filtered out.
        NSArray<LOMember *>*result = aRequest.result;
        NSMutableArray *resultWithoutGuestUser = [NSMutableArray new];
        for (member in result) {
            if (member.memberId != kGuestIdentifer) {
                [resultWithoutGuestUser addObject:member];
            }
        }
        allMembers = resultWithoutGuestUser;
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:TRUE];
        [allMembers sortUsingDescriptors:@[sort]];
        [self sortData:allMembers];
    }];
}

#pragma mark -
#pragma mark Create Member Delegate Methods

- (void)memberSaved:(LOMember *)aMember {
    [self start];
}

#pragma mark -
#pragma mark Member Detail Delegate Methods

- (void)memberDeleted {
    [self start];
}

- (void)memberUpdated {
    [self start];
}

#pragma mark -
#pragma <LOCheckOutRestrictionsDelegate>

- (void)presentInsufficientBalanceAlertForMember:(LOMember *)checkOutMember
                                         andTotalTimeSpent:(NSTimeInterval)interval
                                          fromCheckOutView:(LOCheckOutDialog *)view
                                       andMemberDetailView:(id<LOCheckOutDialogDelegate,LOCheckOutRestrictionsDelegate>)memberView {
    Show_Hud;
    [LOAccountBalanceRequest requestWithMemberIdentifier:@(checkOutMember.memberId)
                                              completion:^(LORESTRequest *aRequest) {
                                                  if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                      LOAccountBalance *balance = aRequest.result;
                                                      // Find Base Mintutes
                                                      [[LOVenueService instance] getVenue:[LOVenueService instance].assignedVenueId language:kEnglishLanguage
                                                                               completion:^(LORESTRequest *aRequest) {
                                                          
                                                          if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                              LOVenue *venue = aRequest.result;
                                                              NSNumber *minutesPerPiastre = venue.piastresFactor;
                                                              LODebt *debt = [LOCheckOutRestrictionsModel isBalanceSufficient:balance forTimeIntervalSpent:interval andMinutesPerPiastre:minutesPerPiastre];
                                                              if (debt.exist) {
                                                                  [self showDebtAlert:debt
                                                                            forMember:checkOutMember
                                                                             fromView:view
                                                                   andMemberDetailView:memberView];
                                                              } else {
                                                                  [self checkOutMemberWithMember:checkOutMember
                                                                                        fromView:view
                                                                             andMemberDetailView:memberView];
                                                              }
                                                          }
                                                      }];
                                                  }
    }];
    
}

- (void)showDebtAlert:(LODebt *)debt
            forMember:(LOMember *)checkOutMember
             fromView:(LOCheckOutDialog *)view
  andMemberDetailView:(id<LOCheckOutDialogDelegate,LOCheckOutRestrictionsDelegate>)memberView {
    Hide_Hud;
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"%@ needs to pay for %@ minutes. Are you sure you want to check out this customer?", @"Check-out alert body"), checkOutMember.fullName, debt.minutes.stringValue];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Insufficient balance", @"Check-out alert title") message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okayAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Check out", @"Check-out alert ok") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self checkOutMemberWithMember:checkOutMember fromView:view andMemberDetailView:memberView];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Check-out alert")
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:okayAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)checkOutMemberWithMember:(LOMember *)checkOutMember
                        fromView:(LOCheckOutDialog *)view
             andMemberDetailView:(id<LOCheckOutDialogDelegate, LOCheckOutRestrictionsDelegate>)memberView{
    
    Show_Hud;
    [[LOMemberService instance] confirmCheckOutMember:checkOutMember
                                             checkOut:view.checkOut
                                           completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        if(aRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
            Show_Error_Deprecated(@"There was an error checking-out this member. Please try again.");
            return;
        }
        
        [memberView memberUpdated:aRequest.result];
        [view dismiss:nil];
    }];
    
}

@end
