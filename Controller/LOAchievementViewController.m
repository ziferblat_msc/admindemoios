//
//  LOAchievementViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievementViewController.h"
#import "LOAchievement.h"
#import "LOAchievementInformationView.h"
#import "LOAchievementLevelsView.h"
#import "LORESTRequest.h"

@interface LOBaseViewController ()

- (void)backPressed;

@end

@implementation LOAchievementViewController

- (id)initWithMember:(LOMember *)aMember achievement:(LOAchievement *)anAchievement {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        originalAchievement = anAchievement;
        self.title = anAchievement.achievementId == 0 ? @"New Achievement" : originalAchievement.name;
        tempAchievement = originalAchievement.copy;
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    [super showBack];
    
    if(originalAchievement.achievementId == 0) {
        [super hideHeaderContent:FALSE];
    }
}

- (UIView *)headerContent {
    if(![member.role isEqualToString:@"GLOBAL"]) {
        return nil;
    }
    
    UIView *container = [[UIView alloc] init];
    
    UIButton *deleteButton = [super addButton:@"Delete"];
    [deleteButton setBackgroundColor:Colour_Red forState:UIControlStateNormal];
    deleteButton.width = 120.0;
    [container addSubview:deleteButton];
    [deleteButton addTarget:self action:@selector(deletePressed) forControlEvents:UIControlEventTouchUpInside];
    [container setSize:CGSizeMake(deleteButton.right, deleteButton.height)];
    
    return container;
}

- (void)deletePressed {
    [UIAlertView showWithTitle:@"Delete Achievement" message:@"Are you sure you wish to delete this achievement?" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1) {
            [self deleteAchievement:originalAchievement];
        }
    }];
}

#pragma mark -
#pragma mark Request Methods

- (void)deleteAchievement:(LOAchievement *)anAchievement {
    Show_Hud;
    
    [[LOAchievementsService instance] deleteAchievement:anAchievement completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        [super backPressed];
    }];
}

#pragma mark -
#pragma mark Edit Methods

- (NSArray *)editViews
{
    CGRect contentFrame = CGRectMake(0, 0, contentScrollview.width, contentScrollview.height);
    
    return @[[[LOAchievementInformationView alloc] initWithFrame:contentFrame],
             [[LOAchievementLevelsView alloc] initWithFrame:contentFrame]];
}

- (NSArray *)editCaptions {
    return @[@"Information", @"Levels"];
}

- (void)showEditView:(NSUInteger)index {
    [super showEditView:index];
    [currentEditView setData:tempAchievement];
    
    if (originalAchievement.achievementId == 0)
    {
        isEditing = YES;
        [currentEditView setEditViewMode:EditViewModeNew];
    }
    [currentEditView layoutViewForEditMode];
}

#pragma mark -
#pragma mark Edit View Delegate Methods

- (void)dataDidUpdate:(LOAchievement *)anAchievement {
    originalAchievement = anAchievement;
    self.title = originalAchievement.achievementId == 0 ? @"New Achievement" : originalAchievement.name;
    [super updateTitle];
    
    if(originalAchievement.achievementId > 0) {
        [self showHeaderContent:TRUE];
    }
}

- (void)didPressCancelButton:(void (^)())completionBlock {
    isEditing = NO;
    [currentEditView setData:originalAchievement];
    tempAchievement = originalAchievement.copy;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

- (void)didPressSavebutton {
    Show_Hud;
    [currentEditView saveTemp:^(id<LOEditableItem> item) {
        if ([item isKindOfClass:[LOAchievement class]]) {
            tempAchievement = (LOAchievement *)item;
            [tempAchievement validateAchievement:^(BOOL success, NSString *errorMessage) {
                if (success) {
                    [self saveAchievement:tempAchievement];
                } else {
                    Hide_Hud;
                    Show_Error(errorMessage);
                }
            }];
        }
    }];
}


#pragma mark -
#pragma mark Request Methods

- (void)saveAchievement:(LOAchievement *)anAchievement
{
    [[LOAchievementsService instance] saveAchievement:anAchievement
                                             language:currentEditView.language
                                           completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed)
        {
            Show_Error(NSLocalizedString(@"Can't update an achivement", @"Error message on update / save"))
            return;
        }
        
        if ([aRequest.result isKindOfClass:[LOAchievement class]]) {
            isEditing = NO;
            [currentEditView setEditViewMode:EditViewModeNormal];
            originalAchievement = aRequest.result;
            tempAchievement = originalAchievement.copy;
            [self dataDidUpdate:originalAchievement];
            [currentEditView updateLastUpdated];
        }
    }];
}

//Language changing

- (void)setupLanguage:(NSString *)languageStr
{
    [super setupLanguage:languageStr];
    
    [[LOAchievementsService instance] getAchievement:originalAchievement.achievementId language:languageStr completion:^(LORESTRequest *aRequest)
    {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        originalAchievement = aRequest.result;
        tempAchievement = originalAchievement.copy;
        [self dataDidUpdate:originalAchievement];
        [currentEditView updateLastUpdated];
        [currentEditView setData:originalAchievement];
    }];
}


@end
