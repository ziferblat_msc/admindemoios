//
//  LOAchievementViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEditViewController.h"

@class LOAchievement;

@interface LOAchievementViewController : LOEditViewController
{
    LOAchievement *originalAchievement;
    LOAchievement *tempAchievement;
}

- (id)initWithMember:(LOMember *)aMember achievement:(LOAchievement *)anAchievement;

@end
