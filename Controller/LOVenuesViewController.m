//
//  LOVenuesViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOVenuesViewController.h"
#import "LOVenueViewController.h"
#import "LOZiferblatCell.h"
#import "LOVenue.h"
#import "LOMember.h"

@implementation LOVenuesViewController

static NSString *ziferblatCellIdentifier = @"ZiferblatCell";

- (id)initWithMember:(LOMember *)aMember {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        self.title = @"Venues";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:self.title image:[UIImage imageNamed:@"VenuesTabIcon"] selectedImage:nil];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(120, 142)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:100.0];
    [flowLayout setMinimumLineSpacing:125.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(75.0, 100.0, 75.0, 100.0)];
    
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, contentView.width, contentView.height) collectionViewLayout:flowLayout];
    [collectionView setBackgroundColor:Colour_Clear];
    [collectionView registerClass:[LOZiferblatCell class] forCellWithReuseIdentifier:ziferblatCellIdentifier];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [contentView addSubview:collectionView];
    
    noDataLabel = [[UILabel alloc] init];
    [noDataLabel setText:@"No venues."];
    [noDataLabel sizeToFit];
    [contentView addSubview:noDataLabel];
    [noDataLabel centerInSuperView];
    noDataLabel.hidden = TRUE;
    
    if([member.role isEqualToString:@"GLOBAL"]) {
        [super showAdd:@"New venue"];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self listVenues];
}

- (UIView *)headerContent
{
    [label removeFromSuperview];
    
    label = [[UILabel alloc] init];
    [label setBackgroundColor:Colour_Clear];
    [label setTextColor:Colour_White];
    [label setFont:Font_Italic(14.0)];
    [label setText:@"Loading..."];
    [label sizeToFit];
    
    return label;
}

- (void)addItem {
    LOVenue *venue = [[LOVenue alloc] init];
    [venue setCurrencyCode:@"GBP"];
    [self editVenue:venue];
}

- (void)editVenue:(LOVenue *)aVenue {
    LOVenueViewController *controller = [[LOVenueViewController alloc] initWithMember:member venue:aVenue];
    [self.navigationController pushViewController:controller animated:TRUE];
}

#pragma mark -
#pragma mark Request Methods

- (void)listVenues
{
    [LONetworkBar showFromView:headerView];

    [[LOVenueService instance] listVenues:member completion:^(LORESTRequest *aRequest) {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        venues = aRequest.result;
        [[LOVenueService instance] setVenues:venues];
        [collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        
        if([member.role isEqualToString:@"GLOBAL"]) {
            [label setText:@"You have access to ALL locations"];
        } else if(venues.count == 1) {
            [label setText:@"You have access to 1 location"];
        } else {
            [label setText:[NSString stringWithFormat:@"You have access to %ld locations", (long)venues.count]];
        }
        
        [label sizeToFit];
        [label centerHorizontallyInSuperView];
        
        noDataLabel.hidden = venues.count > 0;
        collectionView.hidden = venues.count == 0;
    }];
}

- (void)getVenue:(NSUInteger)venueId
{
    [[LOVenueService instance] getVenue:venueId language:kEnglishLanguage completion:^(LORESTRequest *aRequest) {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed)
        {
            //TODO
            return;
        }
        
        [self editVenue:aRequest.result];
    }];
}

#pragma mark -
#pragma mark Collection View Datasource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return venues.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LOZiferblatCell *cell = (LOZiferblatCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ziferblatCellIdentifier                                                                                                         forIndexPath:indexPath];
    
    LOVenue *venue = [venues objectAtIndex:indexPath.row];
    [cell setVenue:venue];
    return cell;
}

#pragma mark -
#pragma mark Collection View Delegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LOVenue *venue = [venues objectAtIndex:indexPath.row];
    [LONetworkBar showFromView:headerView];
    [self getVenue:venue.venueId];
}

@end
