//
//  LODatePickerOverlayChildViewController.m
//  WorkClip
//
//  Created by Danny Bravo a.k.a. Rockstar Developer on 11/07/2014.
//  Copyright (c) 2014 Locassa. All rights reserved.
//

#import "LODatePickerOverlayChildViewController.h"
#import "LODatePickerView.h"
#import "LOOverlayChildView.h"

@implementation LODatePickerOverlayChildViewController
@synthesize tag;

- (instancetype)initWithType:(LOOverlayViewType)aType
                originalDate:(NSDate *)aDate
                       title:(NSString *)aTitle
              datePickerMode:(UIDatePickerMode)aDatePickerMode
                 andDelegate:(id <LOOverlayChildViewControllerDelegate, LODatePickerOverlayChildViewControllerDelegate>)aDelegate
{
    self = [super initWithType:aType andDelegate:aDelegate isPersisted:NO];
    if (self)
    {
        originalDate = aDate;
        title = aTitle;
        
        if (minimumDate && (!originalDate || [minimumDate isLaterThanDate:originalDate])) {
            originalDate = minimumDate;
        } else if (maximumDate && (!originalDate || [maximumDate isEarlierThanDate:originalDate])) {
            originalDate = minimumDate ? minimumDate : maximumDate;
        }
        datePickerDelegate = aDelegate;
        datePickerMode = aDatePickerMode;
    }
    return self;
}

- (void)setupOverlayView
{
    date = originalDate ? originalDate : [NSDate date];
    castedViewToPresent = [[LODatePickerView alloc] initWithTitle:title
                                                                minimumDate:minimumDate
                                                                maximumDate:maximumDate
                                                               selectedDate:date
                                                          andDatePickerMode:datePickerMode];
    [contentView setupWithCastedViewToDisplay:castedViewToPresent backgroundAlpha:0.1 isPersisted:isPersisted];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [castedViewToPresent.pickerView addTarget:self action:@selector(datePickerDidChangeValue:) forControlEvents:UIControlEventValueChanged];
    [datePickerDelegate datePickerController:self didUpdateDate:date];
}

- (void)dismissButtonTapped {
    contentView.userInteractionEnabled = NO;
    [datePickerDelegate datePickerController:self didUpdateDate:(date ? date : originalDate)];
    [self dismissAndPerformBlock:nil];
}


- (void)datePickerDidChangeValue:(UIDatePicker *)datePicker {
    date = [datePicker date];
    [datePickerDelegate datePickerController:self didUpdateDate:date];
}

@end
