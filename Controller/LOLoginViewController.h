//
//  LOLoginViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOBaseViewController.h"

@class LOBlockButton;

@interface LOLoginViewController : LOBaseViewController<UITextFieldDelegate>
{
    UITextField *emailField;
    UITextField *passwordField;
    LOBlockButton *forgotPasswordButton;
    LOBlockButton *signupButton;
    LOBlockButton *backButton;
}

@end
