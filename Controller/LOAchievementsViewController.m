//
//  LOAchievementsViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 02/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievementsViewController.h"
#import "LOAchievementViewController.h"
#import "LOAchievementCell.h"
#import "LOAchievement.h"
#import "LOMember.h"

@implementation LOAchievementsViewController

static NSString *achievementCellIdentifier = @"AchievementCell";

- (id)initWithMember:(LOMember *)aMember {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        self.title = @"Achievements";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:self.title image:[UIImage imageNamed:@"AchievementsTabIcon"] selectedImage:nil];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    
    noDataLabel = [[UILabel alloc] init];
    [noDataLabel setText:@"No achievements."];
    [noDataLabel sizeToFit];
    [contentView addSubview:noDataLabel];
    [noDataLabel centerInSuperView];
    noDataLabel.hidden = TRUE;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(120, 142)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:100.0];
    [flowLayout setMinimumLineSpacing:125.0];
    [flowLayout setSectionInset:UIEdgeInsetsMake(75.0, 100.0, 75.0, 100.0)];
    
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, contentView.width, contentView.height) collectionViewLayout:flowLayout];
    [collectionView setBackgroundColor:Colour_Clear];
    [collectionView registerClass:[LOAchievementCell class] forCellWithReuseIdentifier:achievementCellIdentifier];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [contentView addSubview:collectionView];

    if([member.role isEqualToString:@"GLOBAL"])
    {
        [super showAdd:@"New achievement"];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [LONetworkBar showFromView:headerView];
    [self listAchievements];
}

- (void)addItem {
    LOAchievement *achievement = [[LOAchievement alloc] init];
    [self editAchievement:achievement];
}

- (void)editAchievement:(LOAchievement *)anAchievement {
    LOAchievementViewController *controller = [[LOAchievementViewController alloc] initWithMember:member achievement:anAchievement];
    [self.navigationController pushViewController:controller animated:TRUE];
}

#pragma mark -
#pragma mark Request Methods

- (void)listAchievements {
    [[LOAchievementsService instance] listAchievementsWithCompletion:^(LORESTRequest *aRequest)
    {
        [LONetworkBar dismiss];
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODOs
            return;
        }
        
        achievements = aRequest.result;
        [[LOAchievementsService instance] setAchievements:achievements];
        [collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        noDataLabel.hidden = achievements.count > 0;
        collectionView.hidden = achievements.count == 0;
    }];
}

- (void)getAchievement:(NSUInteger)achievementId
{
    [[LOAchievementsService instance] getAchievement:achievementId language:kEnglishLanguage completion:^(LORESTRequest *aRequest) {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        [self editAchievement:aRequest.result];
    }];
}

#pragma mark -
#pragma mark Collection View Datasource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return achievements.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LOAchievementCell *cell = (LOAchievementCell *)[collectionView dequeueReusableCellWithReuseIdentifier:achievementCellIdentifier                                                                                                         forIndexPath:indexPath];
    
    LOAchievement *achievement = [achievements objectAtIndex:indexPath.row];
    [cell setAchievement:achievement];
    return cell;
}

#pragma mark -
#pragma mark Collection View Delegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LOAchievement *achievement = [achievements objectAtIndex:indexPath.row];
    [LONetworkBar showFromView:headerView];
    [self getAchievement:achievement.achievementId];
}

@end
