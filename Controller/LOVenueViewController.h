//
//  LOVenueViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEditViewController.h"
#import "LOEditView.h"
#import "ZFAlarmClocksView.h"

@class LOVenue;

@interface LOVenueViewController : LOEditViewController <ZFAlarmClocksViewDelegate>
{
    LOVenue *originalVenue;
    LOVenue *tempVenue;
}

@property (strong, nonatomic) NSNumber *venueIdentifier;

- (id)initWithMember:(LOMember *)aMember venue:(LOVenue *)aVenue;

@end
