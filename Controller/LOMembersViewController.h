//
//  LOMembersViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"
#import "LOCreateMemberView.h"
#import "LOMemberDetailView.h"
#import "LOCheckOutRestrictionsDelegate.h"

@class LOMemberGridView;
@class LOMember;

@interface LOMembersViewController : LODashboardItemViewController<UICollectionViewDataSource, UICollectionViewDelegate,
        UISearchBarDelegate, LOCreateMemberViewDelegate, LOMemberDetailViewDelegate, LOCheckOutRestrictionsDelegate>
{
    UICollectionView *collectionView;
    NSMutableArray *allMembers;
    NSArray *members;
}

- (void)sortData:(NSArray *)data;

@end
