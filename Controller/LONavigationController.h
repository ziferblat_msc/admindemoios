//
//  BBNavigationController.h
//  BuaBook
//
//  Created by Danny Bravo a.k.a. Rockstar Developer on 24/06/2014.
//  Copyright (c) 2014 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LONavigationController : UINavigationController

@end
