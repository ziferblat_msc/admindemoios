//
//  LOTabBarController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOTabBarController.h"

@implementation LOTabBarController

- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
}

@end
