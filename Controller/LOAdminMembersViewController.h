//
//  LOAdminMembersViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOMembersViewController.h"
#import "LOAdminMemberView.h"

@interface LOAdminMembersViewController : LOMembersViewController<UISearchBarDelegate, LOAdminMemberViewDelegate> {
    NSArray *venues;
}

@end
