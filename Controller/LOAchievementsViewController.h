//
//  LOAchievementsViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 02/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"

@interface LOAchievementsViewController : LODashboardItemViewController<UICollectionViewDataSource, UICollectionViewDelegate> {
    UICollectionView *collectionView;
    UILabel *noDataLabel;
    NSArray *achievements;
}

@end
