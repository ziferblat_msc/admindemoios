//
//  LOVenueViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOVenueViewController.h"
#import "LOZiferblatInformationView.h"
#import "LOActivitiesView.h"
#import "LOEventsView.h"
#import "LONewsView.h"
#import "LOVenue.h"
#import "LOFinancialsView.h"
#import "ZFAlarmClockViewController.h"

@interface LOBaseViewController ()

- (void)backPressed;

@end

@implementation LOVenueViewController
{
    ZFAlarmClocksView *alarmClocksView;
}

- (id)initWithMember:(LOMember *)aMember venue:(LOVenue *)aVenue {
    self = [super initWithMember:aMember];
    
    if(self != nil)
    {
        originalVenue = aVenue;
        self.venueIdentifier = @(aVenue.venueId);
        self.title = originalVenue.venueId == 0 ? @"New Venue" : originalVenue.name;
        tempVenue = originalVenue.copy;
    }
    
    return self;
}

- (void)loadView
{
    [super loadView];
    [super showBack];
    
    if(originalVenue.venueId == 0) {
        [super hideHeaderContent:FALSE];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if(alarmClocksView)
    {
        [alarmClocksView refreshAlarmClocksList];
    }
}

- (UIView *)headerContent
{
    /*if(![member.role isEqualToString:@"GLOBAL"])
    {
        return nil;
    }*/

    UIView *container = [[UIView alloc] init];
    
    UIButton *assignButton = [super addButton:@"Assign iPad"];
    [assignButton setTitle:@"Unassign iPad" forState:UIControlStateSelected];
    [assignButton setBackgroundColor:Colour_Green forState:UIControlStateSelected];
    assignButton.width = 120.0;
    [container addSubview:assignButton];
    [assignButton addTarget:self action:@selector(assignPressed:) forControlEvents:UIControlEventTouchUpInside];
    [container setSize:CGSizeMake(assignButton.right, assignButton.height)];
    
    UIButton *deleteButton = [super addButton:@"Delete Ziferblat"];
    [deleteButton setBackgroundColor:Colour_Red forState:UIControlStateNormal];
    deleteButton.width = 120.0;
    [container addSubview:deleteButton];
    deleteButton.left = assignButton.right + 90.0;
    [deleteButton addTarget:self action:@selector(deletePressed) forControlEvents:UIControlEventTouchUpInside];
    [container setSize:CGSizeMake(deleteButton.right, deleteButton.height)];
    
    if([[LOVenueService instance] isAssignedToVenue])
    {
        [assignButton setSelected:[[LOVenueService instance] assignedVenueId] == originalVenue.venueId];
    }
    
    return container;
}

- (void)deletePressed {
    [UIAlertView showWithTitle:@"Delete Ziferblat" message:@"Are you sure you wish to delete this Ziferblat?" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1) {
            [self deleteVenue:originalVenue];
        }
    }];
}

- (void)assignPressed:(UIButton *)sender {
    [sender setSelected:!sender.isSelected];
    
    if(sender.isSelected) {
        [[LOVenueService instance] assignPadToVenue:originalVenue];
    } else {
        [[LOVenueService instance] unassignPad];
    }
}

#pragma mark -
#pragma mark Edit Methods

- (NSArray *)editViews
{
    CGRect contentFrame = CGRectMake(0, 0, contentScrollview.width, contentScrollview.height);
    
    LOActivitiesView *activitiesView = [[LOActivitiesView alloc] initWithFrame:contentFrame];
    activitiesView.containerViewController = self;
    
    alarmClocksView = [[ZFAlarmClocksView alloc] initWithFrame:contentFrame];
    alarmClocksView.viewDelegate = self;
    
    return @[[[LOZiferblatInformationView alloc] initWithFrame:contentFrame],
             alarmClocksView,
             [[LOFinancialsView alloc] initWithFrame:contentFrame parentViewController:self],
             activitiesView,
             [[LOEventsView alloc] initWithFrame:contentFrame],
             [[LONewsView alloc] initWithFrame:contentFrame]];
}

- (NSArray *)editCaptions
{
    if([LoggedInMember.role isEqualToString:@"LOCAL_ADMIN"])
    {
        return @[@"Information", @"Guests"];
    }
    else
    {
        return @[@"Information", @"Guests", @"Financials", @"Activities", @"Events", @"News"];
    }
}

- (NSArray *)editViewPermissions
{
    if([LoggedInMember.role isEqualToString:@"LOCAL_ADMIN"])
    {
        return @[ @0,@0 ];
    }
    else
    {
        return @[@1, @0, @1, @0, @0, @0];
    }
}

- (void)showEditView:(NSUInteger)index
{
    [super showEditView:index];
    [currentEditView setData:tempVenue];
    
    if (originalVenue.venueId == 0) {
        isEditing = YES;
        [currentEditView setEditViewMode:EditViewModeNew];
    }
    [currentEditView layoutViewForEditMode];
}

- (BOOL)isNew
{
    return (originalVenue.venueId == 0);
}

#pragma mark -
#pragma mark Request Methods

- (void)saveVenue:(LOVenue *)aVenue
{
    [[LOVenueService instance] saveVenue:aVenue language:currentEditView.language completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        if ([aRequest.result isKindOfClass:[LOVenue class]]) {
            isEditing = NO;
            [currentEditView setEditViewMode:EditViewModeNormal];
            originalVenue = aRequest.result;
            tempVenue = originalVenue.copy;
            [self dataDidUpdate:originalVenue];
            [currentEditView updateLastUpdated];
            [currentEditView setData:originalVenue];
        }
    }];
}

- (void)deleteVenue:(LOVenue *)aVenue
{
    Show_Hud;
    
    [[LOVenueService instance] deleteVenue:originalVenue completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed)
        {
            //TODO
            return;
        }
        
        [super backPressed];
    }];
}

#pragma mark -
#pragma mark Edit View Delegate Methods

- (void)dataDidUpdate:(LOVenue *)aVenue {
    originalVenue = aVenue;
    self.title = originalVenue.venueId == 0 ? @"New Venue" : originalVenue.name;
    [super updateTitle];
    
    if(originalVenue.venueId > 0) {
        [self showHeaderContent:TRUE];
    }
}

- (void)didPressCancelButton:(void (^)())completionBlock {
    isEditing = NO;
    [currentEditView setData:originalVenue];
    tempVenue = originalVenue.copy;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

- (void)didPressSavebutton {
    Show_Hud;
    [currentEditView saveTemp:^(id<LOEditableItem> item) {
        if ([item isKindOfClass:[LOVenue class]]) {
            tempVenue = (LOVenue *)item;
            [tempVenue validateVenue:^(BOOL success, NSString *errorMessage) {
                if (success) {
                    [self saveVenue:tempVenue];
                } else {
                    Hide_Hud;
                    Show_Error(errorMessage);
                }
            }];
        }
    }];
}

#pragma mark -
#pragma mark Alarmc clocks View Delegate Methods

-(void)alarmClockSelected:(ZFAlarmClock *)anAlarmClock
{
    ZFAlarmClockViewController *controller = [[ZFAlarmClockViewController alloc] initWithMember:member alarmClock:anAlarmClock];
    [self.navigationController pushViewController:controller animated:TRUE];
}

//Language changing

- (void)setupLanguage:(NSString *)languageStr
{
    [super setupLanguage:languageStr];
    
    Show_Hud;
    
    [[LOVenueService instance] getVenue:originalVenue.venueId language:languageStr completion:^(LORESTRequest *aRequest)
    {
        Hide_Hud;
        
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed)
        {
            //TODO
            return;
        }
        
        if ([aRequest.result isKindOfClass:[LOVenue class]])
        {
            originalVenue = aRequest.result;
            tempVenue = originalVenue.copy;
            [self dataDidUpdate:originalVenue];
            [currentEditView updateLastUpdated];
            [currentEditView setData:originalVenue];
        }
    }];
}

@end
