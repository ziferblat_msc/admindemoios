//
//  ZFSignUpViewController.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 18.04.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFSignUpViewController.h"
#import "LOBlockButton.h"
#import "LOMember.h"
#import "LODashboardViewController.h"

@implementation ZFSignUpViewController

- (void)loadView
{
    [super loadView];
    
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [passwordTextField setEnablesReturnKeyAutomatically:TRUE];
    [passwordTextField setPlaceholder:NSLocalizedString(@"Password", nil)];
    [passwordTextField setTextAlignment:NSTextAlignmentCenter];
    [passwordTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [passwordTextField setDelegate:self];
    [passwordTextField setSecureTextEntry:TRUE];
    [self.view addSubview:passwordTextField];
    [passwordTextField centerInSuperView];
    
    secretWordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [secretWordTextField setEnablesReturnKeyAutomatically:TRUE];
    [secretWordTextField setPlaceholder:NSLocalizedString(@"Secret word", nil)];
    [secretWordTextField setTextAlignment:NSTextAlignmentCenter];
    [secretWordTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [secretWordTextField setDelegate:self];
    [self.view addSubview:secretWordTextField];
    [secretWordTextField centerHorizontallyInSuperView];
    secretWordTextField.bottom = passwordTextField.top - kPadding;
    
    emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [emailTextField setEnablesReturnKeyAutomatically:TRUE];
    [emailTextField setPlaceholder:NSLocalizedString(@"Email", nil)];
    [emailTextField setTextAlignment:NSTextAlignmentCenter];
    [emailTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [emailTextField setDelegate:self];
    [self.view addSubview:emailTextField];
    [emailTextField centerHorizontallyInSuperView];
    emailTextField.bottom = secretWordTextField.top - kPadding;
    
    confirmPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [confirmPasswordTextField setEnablesReturnKeyAutomatically:TRUE];
    [confirmPasswordTextField setPlaceholder:NSLocalizedString(@"Confirm password", nil)];
    [confirmPasswordTextField setTextAlignment:NSTextAlignmentCenter];
    [confirmPasswordTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [confirmPasswordTextField setDelegate:self];
    [confirmPasswordTextField setSecureTextEntry:TRUE];
    confirmPasswordTextField.returnKeyType = UIReturnKeyGo;
    [self.view addSubview:confirmPasswordTextField];
    [confirmPasswordTextField centerHorizontallyInSuperView];
    confirmPasswordTextField.top = passwordTextField.bottom + kPadding;
    
    backButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [backButton setSize:CGSizeMake(44.0, 44.0)];
    [backButton setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
    [self.view addSubview:backButton];
    [backButton addTarget:self
                   action:@selector(dismissView)
         forControlEvents:UIControlEventTouchUpInside];
    
    backButton.right = emailTextField.left - kLargePadding;
    [backButton setCenterY: emailTextField.center.y];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setFont:Font(24)];
    [titleLabel setTextColor:Colour_LightBlue];
    [titleLabel setBackgroundColor:Colour_Clear];
    [titleLabel setText:@"Become a new Admin member"];
    [titleLabel sizeToFit];
    [self.view addSubview:titleLabel];
    [titleLabel centerHorizontallyInSuperView];
    titleLabel.bottom = emailTextField.top - (kLargePadding);
}

#pragma mark -
#pragma mark Navigation

- (void)dismissView
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)enterToDashBoardWithMember:(LOMember *)member
{
    LODashboardViewController *controller = [[LODashboardViewController alloc] initWithMember:member];
    [self.navigationController pushViewController:controller animated:TRUE];
}

#pragma mark -
#pragma mark Request Methods

- (void)signUp:(NSString *)aUsername secretWord:(NSString *)aSecretWord password:(NSString *)aPassword
{
    [self.view endEditing:YES];
    
    [[LOMemberService instance] signUp:aUsername secretWord:aSecretWord password:aPassword completion:^(LORESTRequest *aRequest)
     {
         if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded)
         {
             LOMember *member = aRequest.result;
             member.password = aPassword;
             
             [[LORESTRequestService instance] setUsername:member.email];
             [[LORESTRequestService instance] setPassword:member.password];
             
             [UserService setLoggedInMember:member];
             
             [self enterToDashBoardWithMember:member];
             
             secretWordTextField.text = @"";
             passwordTextField.text = @"";
         }
         else
         {
             [secretWordTextField setText:nil];
             
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ziferblat" message:@"Invalid email or secret word." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alertView show];
         }
     }];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == emailTextField)
    {
        [secretWordTextField becomeFirstResponder];
        return NO;
    }
    else if(textField == secretWordTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else if(textField == passwordTextField)
    {
        [confirmPasswordTextField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self validateAndTryToSignUp];
        return NO;
    }
    
    return YES;
}

#pragma mark -
#pragma mark Validation

-(void)validateAndTryToSignUp
{
    if(emailTextField.text.length == 0 || secretWordTextField.text.length == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ziferblat" message:@"Invalid email or secret word." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    else if(![passwordTextField.text isEqualToString:confirmPasswordTextField.text])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ziferblat" message:@"Passwords are not equal." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    else if(passwordTextField.text.length < 6 || [passwordTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound || [passwordTextField.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location == NSNotFound)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ziferblat" message:@"Passwords must be a minimum of 6 characters and contain at least one number and character." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        [self signUp:emailTextField.text secretWord:secretWordTextField.text password:passwordTextField.text];
    }
}


#pragma mark -
#pragma mark Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    
    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.top = -150.0;
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    
    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.top = 0.0;
    } completion:nil];
}

@end
