//
//  LOEditViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"
#import "LOEditView.h"
#import "LOMember.h"

typedef NS_ENUM(NSInteger, AchivementMenu) {
    AchivementMenuInformation,
    AchivementMenuLevels
};

@interface LOEditViewController : LODashboardItemViewController<LOEditViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    LOEditView *currentEditView;
    UITableView *menuTableView;
    UIScrollView *contentScrollview;
    UIView *header;
    UISegmentedControl *languageControl;
    NSArray *menuItems;
    NSArray *editViews;
    NSIndexPath *selectIndex;
    NSIndexPath *deselectIndex;
    BOOL isEditing;
}

- (void)showEditView:(NSUInteger)index;
- (NSArray *)editViews;
- (NSArray *)editCaptions;
- (NSArray *)editViewPermissions;
- (BOOL)isNew;

- (void) setupLanguage:(NSString *)languageStr;

@end
