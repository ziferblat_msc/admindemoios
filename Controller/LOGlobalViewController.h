//
//  LOGlobalViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEditViewController.h"

@class LOGlobalData;

@interface LOGlobalViewController : LOEditViewController {
    LOGlobalData *tempGlobalData;
    LOGlobalData *originalGlobalData;
}

@end
