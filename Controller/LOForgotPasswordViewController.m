//
//  LOForgotPasswordViewController.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOForgotPasswordViewController.h"
#import "LONewPasswordViewController.h"
#import "LOBlockButton.h"
#import "LOMember.h"
#import "LOForgotPasswordRequest.h"

@interface LOForgotPasswordViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) LOBlockButton *backButton;
@property (nonatomic, strong) LOBlockButton *submitButton;

@end

@implementation LOForgotPasswordViewController

@synthesize emailTextField;
@synthesize backButton;
@synthesize submitButton;

- (void)loadView {
    [super loadView];
    
    emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [emailTextField setEnablesReturnKeyAutomatically:TRUE];
    [emailTextField setPlaceholder:NSLocalizedString(@"Email", nil)];
    [emailTextField setDelegate:self];
    [emailTextField setTextAlignment:NSTextAlignmentCenter];
    [emailTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [emailTextField setDelegate:self];
    [self.view addSubview:emailTextField];
    [emailTextField centerInSuperView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.text = NSLocalizedString(@"Please enter your email:", nil);
    titleLabel.font = Font(14.0f);
    titleLabel.textColor = Colour_White;
    [titleLabel sizeToFit];
    [self.view addSubview:titleLabel];
    titleLabel.bottom = emailTextField.top - kPadding;
    titleLabel.left = emailTextField.left;
    
    backButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [backButton setSize:CGSizeMake(44.0, 44.0)];
    [backButton setImage:[UIImage imageNamed:@"BackIcon"] forState:UIControlStateNormal];
    [self.view addSubview:backButton];
    [backButton addTarget:self
                   action:@selector(dismissView)
         forControlEvents:UIControlEventTouchUpInside];
    [backButton centerVerticallyInSuperView];
    backButton.right = emailTextField.left - kLargePadding;
    
    NSString *string = NSLocalizedString(@"Submit", nil);
    NSDictionary *attributedDict = @{ NSFontAttributeName : Font_Bold(14.0),
                                      NSForegroundColorAttributeName : [UIColor whiteColor] };
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedTitle addAttributes:attributedDict
                             range:NSMakeRange(0, string.length)];
    submitButton = [LOBlockButton buttonWithType:UIButtonTypeCustom];
    [submitButton setAttributedTitle:attributedTitle
                            forState:UIControlStateNormal];
    [self.view addSubview:submitButton];
    [submitButton addTarget:self
                   action:@selector(submitEmailForForgotPassword)
           forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(200, 100)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:attributedDict
                                       context:nil];
    submitButton.frame = rect;
    double minHeight = 40;
    if (submitButton.height < minHeight) {
        submitButton.height = minHeight;
    }
    submitButton.top = emailTextField.bottom + kPadding;
    submitButton.right = emailTextField.right;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Actions / Selectors

- (void)submitEmailForForgotPassword {
    [self.view endEditing:YES];
    NSString *enteredEmail = emailTextField.text;
    
    if ([enteredEmail isEqualToString:@""])
    {
        Show_Error(@"Please enter a email address");
    }
    else {
        Show_Hud;
        [LOForgotPasswordRequest requestWithEmail:emailTextField.text completion:^(LORESTRequest *aRequest) {
            Hide_Hud;
            if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                [self goToSetNewPassword];
            } else {
                Show_Error(@"Email address does not match with this admin account");
            }
        }];
    }
}

- (void)dismissView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goToSetNewPassword
{
    LONewPasswordViewController *newPasswordController = [[LONewPasswordViewController alloc] initWithEmail:emailTextField.text];
    [self.navigationController pushViewController:newPasswordController animated:YES];
}

#pragma mark - UITextFieldDelegate;

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
