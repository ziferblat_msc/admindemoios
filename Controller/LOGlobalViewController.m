//
//  LOGlobalViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOGlobalViewController.h"
#import "LOEditView.h"
#import "LOGlobalDataView.h"
#import "LOGlobalData.h"
#import "LOComparativeAnalysisRequest.h"

@implementation LOGlobalViewController

- (id)initWithMember:(LOMember *)aMember {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        self.title = @"Global Data";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:self.title image:[UIImage imageNamed:@"GlobalDataTabIcon"] selectedImage:nil];
    }
    
    return self;
}

- (void)loadView {
    [super loadView];
    [self getData];
}

#pragma mark -
#pragma mark Request Methods

- (void)getData {
    Show_Hud;
    
    [[LOGlobalDataService instance] getDataWithCompletion:^(LORESTRequest *aRequest) {
        Hide_Hud;
       
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        originalGlobalData = aRequest.result;
        tempGlobalData = originalGlobalData.copy;
        [self showEditView:0];
    }];
}

#pragma mark -
#pragma mark Edit Methods

- (NSArray *)editViews {
    CGRect contentFrame = CGRectMake(0, 0, contentScrollview.width, contentScrollview.height);
    
    return @[[[LOGlobalDataView alloc] initWithFrame:contentFrame]];
}

- (NSArray *)editCaptions {
    return @[@"General"];
}

- (void)showEditView:(NSUInteger)index {
    [super showEditView:index];
    [currentEditView setData:tempGlobalData];
    [currentEditView layoutViewForEditMode];
}

#pragma mark - LOEditViewDelegate

- (void)didPressCancelButton:(void (^)())completionBlock {
    isEditing = NO;
    [currentEditView setData:originalGlobalData];
    tempGlobalData = originalGlobalData.copy;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

- (void)didPressSavebutton {
    Show_Hud;
    [currentEditView saveTemp:^(id<LOEditableItem> item) {
        if ([item isKindOfClass:[LOGlobalData class]]) {
            tempGlobalData = (LOGlobalData *)item;
            [tempGlobalData validateGlobalData:^(BOOL success, NSString *errorMessage) {
                if (success) {
                    [self saveGlobalData:tempGlobalData];
                } else {
                    Hide_Hud;
                    Show_Error(errorMessage);
                }
            }];
        }
    }];
}

#pragma mark -
#pragma mark Request Methods

- (void)saveGlobalData:(LOGlobalData *)someData {
    [[LOGlobalDataService instance] saveData:someData completion:^(LORESTRequest *aRequest) {
        Hide_Hud;
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            return;
        }
        // Result of global data service current returns string OK when success, not the updated global data object.
        isEditing = NO;
        [currentEditView setEditViewMode:EditViewModeNormal];
        [currentEditView updateLastUpdated];
    }];
}


@end
