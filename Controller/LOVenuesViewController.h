//
//  LOVenuesViewController.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"

@interface LOVenuesViewController : LODashboardItemViewController<UICollectionViewDataSource, UICollectionViewDelegate>
{
    UICollectionView *collectionView;
    NSArray *venues;
    UILabel *label;
    UILabel *noDataLabel;
}

@end
