//
//  LODatePickerOverlayChildViewController.h
//  WorkClip
//
//  Created by Danny Bravo a.k.a. Rockstar Developer on 11/07/2014.
//  Copyright (c) 2014 Locassa. All rights reserved.
//

#import "LOOverlayChildViewController.h"

@class LODatePickerOverlayChildViewController;
@protocol LODatePickerOverlayChildViewControllerDelegate <NSObject>
- (void)datePickerController:(LODatePickerOverlayChildViewController *)aController didUpdateDate:(NSDate *)date;
@end

@class LODatePickerView;
@interface LODatePickerOverlayChildViewController : LOOverlayChildViewController {
    LODatePickerView *castedViewToPresent;
    UIDatePickerMode datePickerMode;
    NSDate *minimumDate;
    NSDate *maximumDate;
    NSDate *originalDate;
    NSDate *date;
    NSString *title;
    __weak id <LODatePickerOverlayChildViewControllerDelegate> datePickerDelegate;
}
@property (nonatomic, unsafe_unretained, readonly) NSInteger tag;

- (instancetype)initWithType:(LOOverlayViewType)aType
                originalDate:(NSDate *)aDate
                       title:(NSString *)aTitle
              datePickerMode:(UIDatePickerMode)aDatePickerMode
                 andDelegate:(id <LOOverlayChildViewControllerDelegate, LODatePickerOverlayChildViewControllerDelegate>)aDelegate;

@end
