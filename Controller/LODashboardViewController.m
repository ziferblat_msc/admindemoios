//
//  LODashboardViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODashboardViewController.h"
#import "LOMemberInfoView.h"
#import "LORESTRequestService.h"
#import "LOCreateMemberView.h"
#import "LOAdminMemberView.h"
#import "LOMemberDetailView.h"
#import "LOVenue.h"
#import "LOMember.h"

#import "LOVenuesViewController.h"
#import "LOMembersViewController.h"
#import "LOAdminMembersViewController.h"
#import "LOAchievementsViewController.h"
#import "LOGlobalViewController.h"
#import "LOStatisticsViewController.h"

@implementation LODashboardViewController

- (id)initWithMember:(LOMember *)aMember {
    self = [super init];
    
    if(self != nil)
    {
        member = aMember;
        
        if([member.role isEqualToString:@"GLOBAL"])
        {
            [self setViewControllers:@[[self wrapController:[LOVenuesViewController class]],
                                       [self wrapController:[LOMembersViewController class]],
                                       [self wrapController:[LOAdminMembersViewController class]],
                                       [self wrapController:[LOAchievementsViewController class]],
                                       [self wrapController:[LOGlobalViewController class]],
                                       [self wrapController:[LOStatisticsViewController class]]]];
        }
        else if([member.role isEqualToString:@"ADMIN"])
        {
            [self setViewControllers:@[[self wrapController:[LOVenuesViewController class]],
                                       [self wrapController:[LOMembersViewController class]],
                                       [self wrapController:[LOAdminMembersViewController class]]]];
        }
        else
        {
            [self setViewControllers:@[[self wrapController:[LOVenuesViewController class]],
                                       [self wrapController:[LOMembersViewController class]]]];
        }
        
        [self.tabBar setTintColor:Colour_DarkBlue];
    }
    
    return self;
}

- (UINavigationController *)wrapController:(Class)class {
    UIViewController *viewController = [[class alloc] initWithMember:member];
    LONavigationController *navigationController = [[LONavigationController alloc] initWithRootViewController:viewController];
    [navigationController setNavigationBarHidden:TRUE];
    return navigationController;
}

@end
