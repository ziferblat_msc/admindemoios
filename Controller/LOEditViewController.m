//
//  LOEditViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEditViewController.h"
#import "LOMenuCell.h"
#import "LODialogView.h"

@implementation LOEditViewController

static NSString *menuIdentifier = @"MenuIdentifier";

- (void)loadView
{
    [super loadView];
    
    menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 225, contentView.height) style:UITableViewStylePlain];
    [menuTableView registerClass:[LOMenuCell class] forCellReuseIdentifier:menuIdentifier];
    [menuTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [menuTableView setAlwaysBounceVertical:FALSE];
    [menuTableView setBackgroundColor:Colour_Control];
    [menuTableView setDataSource:self];
    [menuTableView setDelegate:self];
    [menuTableView setRowHeight:100];
    [contentView addSubview:menuTableView];
    
    contentScrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(menuTableView.width, 0, contentView.width - menuTableView.width, contentView.height)];
    [contentScrollview setBackgroundColor:Colour_White];
    [contentView addSubview:contentScrollview];
    contentScrollview.bottom = contentView.height;
    
    editViews = [self editViews];
    [editViews makeObjectsPerformSelector:@selector(setDelegate:) withObject:self];
    
    menuItems = [self editCaptions];
    [menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:FALSE scrollPosition:UITableViewScrollPositionTop];
    
    //setuping language controller
    languageControl  = [[UISegmentedControl alloc] initWithItems:@[@"English", @"Russian",]];
    languageControl.selectedSegmentIndex = 0;
    [languageControl addTarget:self action:@selector(languageChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self showEditView:0];
    
    Notification_Observe(UIKeyboardWillShowNotification, keyboardWillShow);
}

- (void)dealloc {
    Notification_RemoveObserver;
}

- (void)keyboardWillShow {
    UIView *view = [UIResponder currentFirstResponder];
    
    if ([view class] == [UIAlertController class]) {
        // This is fixing the crash:
        // "[UIAlertController superview]: unrecognized selector sent to instance"
        return;
    }
    
    CGRect frame = [view.superview convertRect:view.frame toView:contentScrollview];
    frame.size.height += (kLargePadding * 1.5);
    
    if([view isDescendantOfView:contentScrollview]) {
        [contentScrollview scrollRectToVisible:frame animated:FALSE];
    }
}

#pragma mark -
#pragma mark Edit Methods

- (NSArray *)editViews {
    return nil;
}

- (NSArray *)editCaptions {
    return nil;
}

- (NSArray *)editViewPermissions {
    return nil;
}

- (void)showEditView:(NSUInteger)index
{
    if([editViews indexOfObject:currentEditView] == index) {
        return;
    }
    
    [currentEditView.editPanel removeFromSuperview];
    currentEditView = [editViews objectAtIndex:index];
    [contentScrollview removeSubviews];
    [contentScrollview addSubview:currentEditView];
    [contentScrollview setContentSize:CGSizeMake(contentScrollview.width, currentEditView.height)];
    
    if(currentEditView.editPanel != nil) {
        [contentView addSubview:currentEditView.editPanel];
        currentEditView.editPanel.right = contentView.width;
        contentScrollview.top = currentEditView.editPanel.height;
        contentScrollview.height = contentView.height - contentScrollview.top;
    } else {
        contentScrollview.top = 0;
        contentScrollview.height = contentView.height;
    }
    
    if (isEditing) {
        [currentEditView setEditViewMode:EditViewModeEditing];
    } else {
        [currentEditView setEditViewMode:EditViewModeNormal];
    }
    
    //MultiLanguage
    if(currentEditView.isMultiLanguage)
    {
        [self showLanguageControl];
    }
    else
    {
        [self removeLanguageControl];
    }
}

- (BOOL)isNew {
    return NO;
}

- (BOOL)viewPermissionForEditViewAtIndex:(NSInteger)index {
    if (index < [self editViewPermissions].count) {
        return [[[self editViewPermissions] objectAtIndex:index] boolValue];
    }
    return YES;
}

#pragma mark -
#pragma mark Show Hide Language Control methods

- (void)showLanguageControl
{
    [languageControl removeFromSuperview];
    
    if(languageControl != nil)
    {
        [contentView addSubview:languageControl];
        
        languageControl.left = menuTableView.right + kLargePadding;
        
        languageControl.top = menuTableView.top + 30;
    }
}

- (void)removeLanguageControl
{
    [languageControl removeFromSuperview];
}

#pragma mark -
#pragma mark Edit View Delegate Methods

- (void)isEditing:(BOOL)value
{
    isEditing = value;
    
    if(isEditing == TRUE)
    {
        languageControl.userInteractionEnabled = FALSE;
        languageControl.alpha = 0.5;
    }
    else
    {
        languageControl.userInteractionEnabled = TRUE;
        languageControl.alpha = 1;
    }
}

- (void)showDialog:(LODialogView *)aDialog
{
    [self.navigationController.tabBarController.view addSubview:aDialog];
    [aDialog show];
}

- (void)dataDidUpdate:(id<LOEditableItem>)someData
{
    //Implementation in child classes
}

#pragma mark -
#pragma mark Table View Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LOMenuCell *cell = (LOMenuCell *)[tableView dequeueReusableCellWithIdentifier:menuIdentifier forIndexPath:indexPath];
    [cell.textLabel setText:[menuItems objectAtIndex:indexPath.row]];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    return cell;
}

#pragma mark -
#pragma mark Table View Delegate Methods

- (void)selectCell:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if(deselectIndex != nil) {
        cell = [menuTableView cellForRowAtIndexPath:deselectIndex];
        [cell setSelected:FALSE];
    }
    
    [menuTableView selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    cell = [menuTableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:TRUE];
    [self showEditView:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(isEditing) {
        if (indexPath.row == AchivementMenuLevels) {
            // Disable swithcing between views while editing to
            // prevent crash when app tries to create level
            // for non existent achivement.
            return;
        }
        if ([self isNew] && ![self viewPermissionForEditViewAtIndex:indexPath.row]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"You must save first" preferredStyle:UIAlertControllerStyleAlert];
            [self presentViewController:alertController animated:YES completion:^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                });
            }];
        } else {
            selectIndex = indexPath;
            [menuTableView deselectRowAtIndexPath:indexPath animated:FALSE];
            
            [currentEditView saveTemp:^(id<LOEditableItem> item) {
            }];
            [self selectCell:selectIndex];
        }
    } else {
        [self selectCell:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(isEditing) {
        deselectIndex = indexPath;
    } else {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setSelected:FALSE];
    }
}

//LanguageControl events

- (void) languageChanged:(id)sender
{
    if(languageControl.selectedSegmentIndex == 0)
    {
        [self setupLanguage:kEnglishLanguage];
    }
    else if(languageControl.selectedSegmentIndex == 1)
    {
        [self setupLanguage:kRussianLanguage];
    }
}

//Setup New Language

- (void) setupLanguage:(NSString *)languageStr
{
    currentEditView.language = languageStr;
    
    //Implementation in child classes
}

@end
