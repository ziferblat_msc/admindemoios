//
//  ZFAlarmClockViewController.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LODashboardItemViewController.h"
#import "ZFAlarmClock.h"
#import "ZFNewGuestDialogView.h"
#import "LOCheckOutDialog.h"

@interface ZFAlarmClockViewController : LODashboardItemViewController<UICollectionViewDataSource, UICollectionViewDelegate,ZFNewGuestDialogViewDelegate,LOCheckOutDialogDelegate,LOCheckOutRestrictionsDelegate>
{
    ZFAlarmClock *alarmClock;
    LOVenue *venue;
    //UILabel *label;
}

@property (nonatomic, strong) UILabel *noDataLabel;
@property (nonatomic, strong) UICollectionView *collectionView;

- (id)initWithMember:(LOMember *)aMember alarmClock:(ZFAlarmClock *)anAlarmClock;

@end
