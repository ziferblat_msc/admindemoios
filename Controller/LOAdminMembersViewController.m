//
//  LOAdminMembersViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAdminMembersViewController.h"
#import "LOMember.h"

@interface LOMembersViewController ()

- (void)start;
- (void)listMembers;

@end

@implementation LOAdminMembersViewController

- (id)initWithMember:(LOMember *)aMember {
    self = [super initWithMember:aMember];
    
    if(self != nil) {
        self.title = @"Admin Members";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:self.title image:[UIImage imageNamed:@"AdminMembersTabIcon"] selectedImage:nil];
        [self setupNotificationObservers];
    }
    
    return self;
}

- (NSString *)newItemCaption {
    return @"New admin member";
}

- (UIView *)headerContent {
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 300, 60)];
    [searchBar setPlaceholder:@"Search admin members"];
    [searchBar setBackgroundImage:[UIImage new]];
    [searchBar setDelegate:self];
    return searchBar;
}

- (void)start {
    [LONetworkBar showFromView:headerView];
    [self listVenuesForMember:member];
    [self listMembers];
}

- (void)addItem {
    LOMember *newMember = [[LOMember alloc] init];
    [self showMember:newMember];
}

- (void)showMember:(LOMember *)aMember {
    LOAdminMemberView *createAdminMemberView = [[LOAdminMemberView alloc] initWithVenues:venues member:aMember];
    [createAdminMemberView setDelegate:self];
    [self.navigationController.tabBarController.view addSubview:createAdminMemberView];
    [createAdminMemberView show];
}

#pragma mark -
#pragma mark Requst Methods

- (void)listVenuesForMember:(LOMember *)aMember {
    [[LOVenueService instance] listVenues:aMember completion:^(LORESTRequest *aRequest) {
        [LONetworkBar dismiss];
        
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        venues = aRequest.result;
    }];
}

- (void)listMembers {
    [[LOMemberService instance] listAllAdminsWithCompletion:^(LORESTRequest *aRequest)
    {
        [LONetworkBar dismiss];
        if(aRequest.resultStatus == LORESTRequestResultStatusFailed) {
            //TODO
            return;
        }
        
        allMembers = [NSMutableArray arrayWithArray:aRequest.result];
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:TRUE];
        [allMembers sortUsingDescriptors:@[sort]];
        [super sortData:allMembers];
    }];
}

#pragma mark -
#pragma mark Create Member Delegate Methods

- (void)adminMemberCreated {
    [self start];
}

- (void)adminMemberDeleted {
    [self start];
}

#pragma mark - Notifications

- (void)setupNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationUserDidChange
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      [self listMembers];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
