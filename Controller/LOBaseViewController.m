//
//  LOBaseViewController.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOBaseViewController.h"

@implementation LOBaseViewController

- (void)loadView {
    [super loadView];
    [self.view setFrame:kFullScreenFrame];
    
    UIImage *backgroundImage = [UIImage imageNamed:@"Background"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.view addSubview:imageView];
    
}

- (void)viewDidAppear:(BOOL)animated {
    Notification_Observe(UIKeyboardWillShowNotification, keyboardWillShow:);
    Notification_Observe(UIKeyboardWillHideNotification, keyboardWillHide:);
}

- (void)viewDidDisappear:(BOOL)animated {
    Notification_RemoveObserver;
}

- (void)dealloc {
    Notification_RemoveObserver;
}

#pragma mark -
#pragma mark Layout Methods

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    return TRUE;
}

#pragma mark -
#pragma mark Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification {
    UIView *item = [UIResponder currentFirstResponder];
    NSLog(@"%@", NSStringFromClass([self class]));
    
    NSDictionary *info = [notification userInfo];
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self layoutForItem:item];
}

- (void)layoutForItem:(UIView *)anItem {
    if([anItem isEqual:currentItem]) {
        [self performSelector:@selector(layoutForItem:) withObject:[UIResponder currentFirstResponder] afterDelay:0.1];
        return;
    }
    
    currentItem = anItem;
    if ([currentItem class] == [UIAlertController class])
    {
        // This is fixing the crash:
        // "[UIAlertController superview]: unrecognized selector sent to instance"
        return;
    }
    
    CGRect frame = [currentItem.superview convertRect:currentItem.frame toView:self.view.window];
    CGFloat offset = MIN(0, (self.view.height - keyboardSize.height) - (frame.origin.y + frame.size.height) - (kLargePadding * 2.0));
    
    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.window.top = offset;
    } completion:^(BOOL finished) {
        [self performSelector:@selector(layoutForItem:) withObject:[UIResponder currentFirstResponder] afterDelay:0.1];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    currentItem = nil;
    
    NSDictionary *info = [notification userInfo];
    
    [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&curve];
    [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    
    [UIView animateWithDuration:duration delay:0.0 options:curve animations:^{
        self.view.window.top = 0;
    } completion:nil];
}

@end
