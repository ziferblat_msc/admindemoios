//
//  BBNavigationController.m
//  BuaBook
//
//  Created by Danny Bravo a.k.a. Rockstar Developer on 24/06/2014.
//  Copyright (c) 2014 Locassa. All rights reserved.
//

#import "LONavigationController.h"

@implementation LONavigationController

- (BOOL)shouldAutorotate {
    return [self.viewControllers.lastObject shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.viewControllers.lastObject supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.viewControllers.lastObject preferredInterfaceOrientationForPresentation];
}

@end
