//
//  LOAchievement.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievement.h"
#import "LOAchievementLevel.h"

@implementation LOAchievement

@synthesize achievementId;
@synthesize name;
@synthesize detail;
@synthesize imageUrl;
@synthesize levels;
@synthesize lastUpdated;

- (id)init {
    self = [super init];
    
    if(self != nil) {
        [super addMappingFromKey:@"id" toKey:@"achievementId"];
        [super addMappingFromKey:@"description" toKey:@"detail"];
        
        [super ignoreEncodeKey:@"venueId"];
        [super ignoreEncodeKey:@"levels"];
        [super ignoreEncodeKey:@"lastUpdated"];
        
        [super addClassMappingForKey:@"levels" class:[LOAchievementLevel class]];
    }
    
    return self;
}

- (void)setLastUpdated:(NSObject *)aValue {
    NSDate *date = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        date = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        date = (NSDate *)aValue;
    }
    
    lastUpdated = date;
}

- (void)setLevels:(NSArray *)someLevels {
    NSMutableArray *sortableLevels = [[NSMutableArray alloc] initWithArray:someLevels];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"sequenceIndex" ascending:TRUE];
    [sortableLevels sortUsingDescriptors:@[sort]];
    levels = sortableLevels;
}

- (void)levelSaved:(LOAchievementLevel *)aLevel {
    NSMutableArray *newLevels = [NSMutableArray arrayWithArray:self.levels];
    BOOL isUpdate = FALSE;
    
    for(LOAchievementLevel *level in self.levels) {
        if(level.achievementLevelId == aLevel.achievementLevelId) {
            [newLevels removeObject:level];
            [newLevels addObject:aLevel];
            isUpdate = TRUE;
            break;
        }
    }
    
    if(!isUpdate) {
        [newLevels addObject:aLevel];
    }

    [self setLevels:newLevels];
}

- (void)levelDeleted:(LOAchievementLevel *)aLevel {
    NSMutableArray *newLevels = [NSMutableArray arrayWithArray:self.levels];
    
    for(LOAchievementLevel *level in self.levels) {
        if(level.achievementLevelId == aLevel.achievementLevelId) {
            [newLevels removeObject:level];
            break;
        }
    }

    [self setLevels:newLevels];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        [copy setAchievementId:self.achievementId];
        [copy setName:self.name];
        [copy setDetail:self.detail];
        [copy setImageUrl:self.imageUrl];
        [copy setLevels:self.levels];
        [copy setLastUpdated:self.lastUpdated];
    }
    return copy;
}

- (void)validateAchievement:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock {
    NSString *nameString = [self.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *imageURLString = [self.imageUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableString *message = [NSMutableString string];
    
    if(nameString.length == 0) {
        [message appendString:@"Please enter a name."];
    }
    
    if(imageURLString.length == 0) {
        if(message.length > 0) {
            [message appendString:@"\n"];
        }
        
        [message appendString:@"Please add an image."];
    }
    
    if(message.length > 0) {
        validateResultBlock(NO, message);
    } else {
        validateResultBlock(YES, @"");
    }
}

@end
