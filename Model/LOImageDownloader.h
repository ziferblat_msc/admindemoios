//
//  LOImageDownloader.h
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 26/01/2016.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LOImageDownloadCompletion)(UIImage *downloadedImage);

@interface LOImageDownloader : NSObject

+ (void)downloadImageFromPath:(NSString*)path
         withCompletionHander:(LOImageDownloadCompletion)downloadComplete;

@end
