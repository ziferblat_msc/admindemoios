//
//  LOUIHelper.h
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 26/01/2016.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LOUIHelper : NSObject

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
