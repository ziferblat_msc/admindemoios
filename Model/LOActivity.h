//
//  LOActivity.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"
#import "LOQueuePosition.h"
#import "LOActivity.h"


@interface LOActivity : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic, strong) NSDate *lastUpdated;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDate *deadLine;
@property (nonatomic, strong) NSDate *dateCreated;

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic) NSUInteger activityId;
@property (nonatomic) NSUInteger likeCount;
@property (nonatomic) NSUInteger peopleNeeded;
@property (nonatomic) NSUInteger commentCount;
@property (nonatomic) NSUInteger attendeeCount;
@property (nonatomic) NSUInteger rewardExperience;
@property (nonatomic) NSUInteger rewardPiastres;
@property (nonatomic) NSUInteger rewardTime;
@property (nonatomic, strong) NSNumber *position;
@property (nonatomic, strong) NSArray<LOMember *> *attendees;
@property (nonatomic, strong) NSArray<LOQueuePosition *> *queue;
@property (nonatomic, strong) NSArray<LOMember *> *completedList;
@property (assign) bool completed;
@property (assign) bool full;
@property (assign) bool commented;
@property (assign) bool recurring;
@property (assign) bool joined;
@property (assign) bool attended;
@property (assign) bool liked;

@property (nonatomic, strong) LOVenue *venue;

- (void)validateActivity:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;

@end
