//
//  LOUpdateAccountBalanceRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;

@interface LOAddPiastresRequest : LORESTRequest

+ (id)requestWithMember:(LOMember *)aMember amount:(NSUInteger)anAmount completion:(LORESTRequestCompletion)aCompletion;

@end
