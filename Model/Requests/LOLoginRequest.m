//
//  LOLoginRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOLoginRequest.h"
#import "LOMember.h"

@implementation LOLoginRequest

#define kLoginUrl   kBaseUrl    @"member/login?email=%@&password=%@"

+ (id)requestWithUsername:(NSString *)aUsername password:(NSString *)aPassword completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kLoginUrl, aUsername, aPassword];
    LOLoginRequest *request = [[LOLoginRequest alloc] initWithURL:url resultClass:[LOMember class] method:LORESTRequestMethodGET data:nil completion:aCompletion];
    [request setUsername:aUsername];
    [request setPassword:aPassword];
    return request;
}

@end
