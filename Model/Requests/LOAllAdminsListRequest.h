//
//  LOAllAdminsListRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOAllAdminsListRequest : LORESTRequest

+ (id)requestWithCompletion:(LORESTRequestCompletion)aCompletion;

@end
