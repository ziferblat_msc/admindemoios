//
//  LODeleteNewsRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LODeleteNewsRequest.h"
#import "LOVenue.h"
#import "LONews.h"

@implementation LODeleteNewsRequest

#define kDeleteNewsUrl   kBaseUrl    @"venue/%ld/news/%ld"

+ (id)requestWithVenue:(LOVenue *)aVenue news:(LONews *)anNews completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kDeleteNewsUrl, (long)aVenue.venueId, (long)anNews.newsId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
