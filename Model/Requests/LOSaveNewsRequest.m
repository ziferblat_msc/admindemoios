//
//  LOSaveNewsRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOSaveNewsRequest.h"
#import "LOVenue.h"
#import "LONews.h"

@implementation LOSaveNewsRequest

#define kUpdateNewsUrl   kBaseUrl    @"venue/%ld/news/%@"

+ (id)requestWithVenueId:(NSUInteger)venueId news:(LONews *)anNews language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kUpdateNewsUrl, (unsigned long)venueId, languageStr];
    LORESTRequest *request = nil;
    
    if(anNews.newsId == 0) {
        request = [LORESTRequest postURL:url resultClass:[LONews class] data:anNews completion:aCompletion];
    } else {
        request = [LORESTRequest putURL:url resultClass:[LONews class] data:anNews completion:aCompletion];
    }
    
    return request;
}

@end
