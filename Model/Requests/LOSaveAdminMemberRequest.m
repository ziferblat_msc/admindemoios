//
//  LOSaveAdminMemberRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveAdminMemberRequest.h"
#import "LOMember.h"

@implementation LOSaveAdminMemberRequest

#define kCreateAdminMemberUrl   kBaseUrl    @"member/admin"
#define kUpdateAdminMemberUrl   kBaseUrl    @"member/admin/%ld"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = nil;
    LORESTRequest *request = nil;
    
    if(aMember.memberId == 0) {
        url = kCreateAdminMemberUrl;
        
        request = [LORESTRequest postURL:url resultClass:[LOMember class] data:aMember completion:aCompletion];
    } else {
        url = [NSString stringWithFormat:kUpdateAdminMemberUrl, (unsigned long)aMember.memberId];
        request = [LORESTRequest putURL:url resultClass:[LOMember class] data:aMember completion:aCompletion];
    }
    
    return request;
}

@end
