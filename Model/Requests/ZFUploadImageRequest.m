//
//  ZFUploadImageRequest.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 28.03.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFUploadImageRequest.h"

#define kImageUploadUrl   kBaseUrl    @"image/upload"

@implementation ZFUploadImageRequest

+ (id)requestWithImage:(UIImage *)image filename:(NSString *)filename category:(NSString *)category completion:(LORESTRequestCompletion)aCompletion
{
    NSString *imageDataString = [UIImageJPEGRepresentation(image, 1.0) base64EncodedStringWithOptions:0];
    
    NSDictionary *dict = @{ @"data" : imageDataString,
                            @"filename" : [filename stringByDeletingPathExtension],
                            @"category" : category,
                            @"extension" : @".jpg",
                            @"width" : [NSString stringWithFormat: @"%d", (int)image.size.width],
                            @"height": [NSString stringWithFormat: @"%d", (int)image.size.height]};
    
    NSData *body = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    ZFUploadImageRequest *request = [[ZFUploadImageRequest alloc] initWithURL:kImageUploadUrl
                                                                  resultClass:nil
                                                                       method:LORESTRequestMethodPOST
                                                                         data:body
                                                                    completion:aCompletion];
    
    return request;
}

@end
