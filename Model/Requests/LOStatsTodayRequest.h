//
//  LOStatsTodayRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOStatsTodayRequest : LORESTRequest

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                           sittingNow:(BOOL)sittingNow
                       includeSitting:(BOOL)includeSitting
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion;

@end
