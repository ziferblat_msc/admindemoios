//
//  ZFAddGuestRequest.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFAddGuestRequest.h"
#import "ZFAlarmClock.h"

@implementation ZFAddGuestRequest

#define kAddGuestUrl   kBaseUrl    @"alarm/users/%lu"

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId guests:(NSArray *)guests completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kAddGuestUrl, (unsigned long)alarmClockId];
    return [LORESTRequest postURL:url resultClass:[ZFAlarmClock class] array:guests completion:aCompletion];
}

@end
