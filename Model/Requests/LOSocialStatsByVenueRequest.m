//
//  LOSocialStatsByVenueRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOSocialStatsByVenueRequest.h"
#import "LOStatsSocialChart.h"

@implementation LOSocialStatsByVenueRequest

#define kStatsSocialUrl  kBaseUrl @"stats/social?venueIds=%@"

+ (instancetype)requestStatsAtVenues:(NSArray *)venuesArray completion:(LORESTRequestCompletion)aCompletion {
    NSMutableString *venueStrings = [[NSMutableString alloc] initWithString:@""];
    for (id venudId in venuesArray) {
        [venueStrings appendString:[NSString stringWithFormat:@"%@", venudId]];
        if (![venudId isEqual:[venuesArray lastObject]]) {
            [venueStrings appendString:@","];
        }
    }
    
    NSString *url = [NSString stringWithFormat:kStatsSocialUrl, venueStrings];
    return [LORESTRequest getURL:url resultClass:[LOStatsSocialChart class] completion:aCompletion];
}

@end
