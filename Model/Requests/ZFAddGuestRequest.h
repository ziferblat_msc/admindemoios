//
//  ZFAddGuestRequest.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface ZFAddGuestRequest : LORESTRequest

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId guests:(NSArray *)guests completion:(LORESTRequestCompletion)aCompletion;

@end
