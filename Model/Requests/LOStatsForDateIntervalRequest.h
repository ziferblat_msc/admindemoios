//
//  LOStatsForDateIntervalRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "LOSocialRequestConstants.h"
#import "LOStatsCurrent.h"

@interface LOStatsForDateIntervalRequest : LORESTRequest

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                           completion:(LORESTRequestCompletion)aCompletion;

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                           memberTypes:(NSArray *)memberType
                           completion:(LORESTRequestCompletion)aCompletion;

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion;

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                           memberTypes:(NSArray *)memberType
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion;

@end
