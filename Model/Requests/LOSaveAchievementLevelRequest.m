//
//  LOSaveAchievementLevelRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveAchievementLevelRequest.h"
#import "LOAchievementLevel.h"
#import "LOAchievement.h"

@implementation LOSaveAchievementLevelRequest

#define kCreateAchievementLevelUrl   kBaseUrl    @"achievements/level/%ld/"
#define kUpdateAchievementLevelUrl   kBaseUrl    @"achievements/level/%ld/%ld/en/"

+ (id)requestWithAchievementLevel:(LOAchievementLevel *)anAchievementLevel forAchievement:(LOAchievement *)anAchievement completion:aCompletion {
    NSString *url = nil;
    LORESTRequest *request = nil;
    
    if(anAchievementLevel.achievementLevelId == 0) {
        url = [NSString stringWithFormat:kCreateAchievementLevelUrl, (unsigned long)anAchievement.achievementId];
        request = [LORESTRequest postURL:url resultClass:[LOAchievementLevel class] data:anAchievementLevel completion:aCompletion];
    } else {
        url = [NSString stringWithFormat:kUpdateAchievementLevelUrl, (unsigned long)anAchievement.achievementId, (unsigned long)anAchievementLevel.achievementLevelId];
        request = [LORESTRequest putURL:url resultClass:[LOAchievementLevel class] data:anAchievementLevel completion:aCompletion];
    }
    
    return request;
}

@end
