//
//  LOActivitiesListRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;

@interface LOActivitiesListRequest : LORESTRequest

+ (id)requestWithVenueIdentifier:(NSNumber *)venueNumber
                            page:(NSInteger)page
                       fetchSize:(NSInteger)fetchSize
                      completion:(LORESTRequestCompletion)aCompletion;

@end
