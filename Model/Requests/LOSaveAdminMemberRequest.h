//
//  LOSaveAdminMemberRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;

@interface LOSaveAdminMemberRequest : LORESTRequest

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion;

@end
