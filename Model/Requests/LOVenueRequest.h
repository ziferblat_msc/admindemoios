//
//  LOVenueRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 15/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOVenueRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)anId language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
