//
//  LOAverageStatsTodayRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOAverageStatsTodayRequest.h"
#import "LOStatsAverage.h"

@implementation LOAverageStatsTodayRequest

#define kAverageStatsTodayUrl  kBaseUrl @"stats/%ld/today/average?includeSitting=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kAverageStatsTodayUrl, (long)venueId, (long)@(includeSitting).integerValue];
    return [LORESTRequest getURL:url resultClass:[LOStatsAverage class] completion:aCompletion];
}

@end
