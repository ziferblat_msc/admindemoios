//
//  LOAccountBalanceRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAccountBalanceRequest.h"
#import "LOAccountBalance.h"
#import "LOMember.h"

@implementation LOAccountBalanceRequest

#define kAccountBalanceUrl   kBaseUrl    @"balance/%@/"

+ (id)requestWithMemberIdentifier:(NSNumber *)memberId
                       completion:(LORESTRequestCompletion)aCompletion {
    
    NSString *url = [NSString stringWithFormat:kAccountBalanceUrl, memberId.stringValue];
    LORESTRequest *request = [LORESTRequest getURL:url resultClass:[LOAccountBalance class] completion:aCompletion];
    return request;
}

@end
