//
//  ZFSaveAlarmClockRequest.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFSaveAlarmClockRequest.h"

@implementation ZFSaveAlarmClockRequest

#define kSaveAlarmClockUrl   kBaseUrl    @"/alarm/register/%lu"

+ (id)requestWithVenueId:(NSUInteger)venueId alarmClock:(ZFAlarmClock *)anAlarmClock completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kSaveAlarmClockUrl, (unsigned long)venueId];
    return [LORESTRequest postURL:url resultClass:[ZFAlarmClock class] data:anAlarmClock completion:aCompletion];
}

@end
