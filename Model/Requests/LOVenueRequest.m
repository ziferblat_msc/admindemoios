//
//  LOVenueRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 15/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOVenueRequest.h"
#import "LOVenue.h"

@implementation LOVenueRequest

#define kVenueUrl   kBaseUrl    @"venue/%ld/%@/?summary=false"

+ (id)requestWithVenueId:(NSUInteger)anId language:(NSString *)languageStr  completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kVenueUrl, (long)anId, languageStr];
    return [LOVenueRequest getURL:url resultClass:[LOVenue class] completion:aCompletion];
}

@end
