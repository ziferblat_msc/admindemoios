//
//  LOAchievementListRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievementListRequest.h"
#import "LOAchievement.h"

@implementation LOAchievementListRequest

#define kAchievementListUrl kBaseUrl    @"achievements/en"

+ (id)requestWithCompletion:(LORESTRequestCompletion)aCompletion {
    LORESTRequest *request = [LORESTRequest getURL:kAchievementListUrl resultClass:[LOAchievement class] completion:aCompletion];
    return request;
}

@end
