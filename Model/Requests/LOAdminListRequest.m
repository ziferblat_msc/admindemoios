//
//  LOGlobalAdminRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAdminListRequest.h"
#import "LOMember.h"

@implementation LOAdminListRequest

#define kGlobalAdminUrl   kBaseUrl    @"member/global"
#define kVenueAdminUrl    kBaseUrl    @"venue/%ld/admins"

+ (id)requestWithVenueId:(NSInteger)aVenueId completion:(LORESTRequestCompletion)aCompletion {
    if(aVenueId == 0) {
        return [LOAdminListRequest getURL:kGlobalAdminUrl resultClass:[LOMember class] completion:aCompletion];
    }
    
    return [LOAdminListRequest getURL:[NSString stringWithFormat:kVenueAdminUrl, (long)aVenueId] resultClass:[LOMember class] completion:aCompletion];
}

@end
