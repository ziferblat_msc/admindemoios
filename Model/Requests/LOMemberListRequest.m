//
//  LOMemberListRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOMemberListRequest.h"
#import "LOMember.h"

@implementation LOMemberListRequest

#define kMemberListUrl   kBaseUrl    @"member"

+ (id)requestWithCompletion:(LORESTRequestCompletion)aCompletion {
    LORESTRequest *request = [LORESTRequest getURL:kMemberListUrl resultClass:[LOMember class] completion:aCompletion];
    return request;
}

@end
