//
//  LODeleteMemberRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODeleteMemberRequest.h"
#import "LOMember.h"

@implementation LODeleteMemberRequest

#define kDeleteMemberUrl   kBaseUrl    @"member/%ld"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteMemberUrl, (long)aMember.memberId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
