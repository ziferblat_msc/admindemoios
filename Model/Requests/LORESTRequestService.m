//
//  LORequestService.m
//  Locassa
//
//  Created by  on 29/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import "LORESTRequestService.h"

@implementation LORESTRequestService

@synthesize username;
@synthesize password;

static LORESTRequestService *instance = nil;

#pragma mark -
#pragma mark Singleton Methods

+ (LORESTRequestService *)instance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void)addRequest:(LORESTRequest *)aRequest {
    [[self instance] addRequest:aRequest];
}

+ (void)requestDidComplete:(LORESTRequest *)aRequest {
    [[self instance] requestDidComplete:aRequest];
}

+ (void)logout {
    [[self instance] logout];
}

#pragma mark -
#pragma mark Instance Methods

- (id)init {
    self = [super init];
    
    if(self != nil) {
        requests = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)logout {
    username = nil;
    password = nil;
    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kLogoutUrl]] delegate:nil];
}

- (void)addRequest:(LORESTRequest *)aRequest {
    [requests addObject:aRequest];
}

- (void)requestDidComplete:(LORESTRequest *)aRequest {
    [requests removeObject:aRequest];
}

@end
