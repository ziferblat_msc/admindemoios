//
//  LODeleteActivityRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LODeleteActivityRequest.h"
#import "LOVenue.h"
#import "LOActivity.h"

@implementation LODeleteActivityRequest

#define kDeleteActivityUrl   kBaseUrl    @"venue/%ld/activity/%ld"

+ (id)requestWithVenue:(LOVenue *)aVenue activity:(LOActivity *)anActivity completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteActivityUrl, (long)aVenue.venueId, (long)anActivity.activityId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
