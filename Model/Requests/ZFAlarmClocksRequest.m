//
//  ZFAlarmClocksRequest.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFAlarmClocksRequest.h"
#import "ZFAlarmClock.h"

@implementation ZFAlarmClocksRequest

#define kAlarmClocksUrl  kBaseUrl  @"alarm/%lu"

+ (id)requestWithVenueId:(NSUInteger)venueId completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kAlarmClocksUrl, (unsigned long)venueId];
    return [LORESTRequest getURL:url resultClass:[ZFAlarmClock class] completion:aCompletion];
}

@end
