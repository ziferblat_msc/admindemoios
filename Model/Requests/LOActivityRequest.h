//
//  LOActivityRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 30/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOActivityRequest : LORESTRequest

+ (id)requestWithVenue:(NSNumber *)venueIdentifier
            activityId:(NSInteger)activityId
              language:(NSString *)languageStr
            completion:(LORESTRequestCompletion)aCompletion;

@end
