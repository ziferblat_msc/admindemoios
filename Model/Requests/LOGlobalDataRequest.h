//
//  LOGlobalDataRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOGlobalDataRequest : LORESTRequest

+ (instancetype)requestWithCompletion:(LORESTRequestCompletion)aCompletion;

@end
