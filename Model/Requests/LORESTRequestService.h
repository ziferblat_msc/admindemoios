//
//  LORequestService.h
//  Locassa
//
//  Created by  on 29/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LORESTRequest.h"

@interface LORESTRequestService : NSObject {
    NSMutableArray *requests;
    NSString *username;
    NSString *password;
}

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

+ (LORESTRequestService *)instance;

+ (void)addRequest:(LORESTRequest *)aRequest;
+ (void)requestDidComplete:(LORESTRequest *)aRequest;
+ (void)logout;

- (void)addRequest:(LORESTRequest *)aRequest;
- (void)requestDidComplete:(LORESTRequest *)aRequest;
- (void)logout;

@end
