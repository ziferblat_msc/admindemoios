//
//  LOChangeAdminPasswordRequest.h
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOChangeAdminPasswordRequest : LORESTRequest

+ (id)requestWithAdminMember:(LOMember *)adminMember oldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword completion:(LORESTRequestCompletion)aCompletion;

@end
