//
//  LOSaveVenueRequest.m
//  ZiferblatAdmin
//
//  Created by Administrator on 16/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveVenueRequest.h"
#import "LOVenue.h"

@implementation LOSaveVenueRequest

#define kCreateVenueUrl   kBaseUrl    @"venue/%@"
#define kUpdateVenueUrl   kBaseUrl    @"venue/%ld/%@"

+ (id)requestWithVenue:(LOVenue *)aVenue language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = nil;
    LORESTRequest *request = nil;
    
    if(aVenue.venueId == 0) {
        url = [NSString stringWithFormat: kCreateVenueUrl, languageStr];
        request = [LORESTRequest postURL:url resultClass:[LOVenue class] data:aVenue completion:aCompletion];
    } else {
        url = [NSString stringWithFormat:kUpdateVenueUrl, (unsigned long)aVenue.venueId, languageStr];
        request = [LORESTRequest putURL:url resultClass:[LOVenue class] data:aVenue completion:aCompletion];
    }
    return request;
}

@end
