//
//  ZFDeleteAlarmClock.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFDeleteAlarmClock.h"

@implementation ZFDeleteAlarmClock

#define kDeleteAlarmClockUrl   kBaseUrl    @"/alarm/remove/%lu"

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kDeleteAlarmClockUrl, (unsigned long)alarmClockId];
    return [LORESTRequest getURL:url resultClass:nil completion:aCompletion];
}

@end
