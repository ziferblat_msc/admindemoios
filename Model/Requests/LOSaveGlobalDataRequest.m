//
//  LOSaveGlobalDataRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveGlobalDataRequest.h"

@implementation LOSaveGlobalDataRequest

#define kSaveGlobalDataUrl  kBaseUrl @"global/"

+ (instancetype)requestWithGlobalData:(LOGlobalData *)someData completion:(LORESTRequestCompletion)aCompletion {
    return [LORESTRequest postURL:kSaveGlobalDataUrl resultClass:nil data:someData completion:aCompletion];
}

@end
