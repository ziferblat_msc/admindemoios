//
//  LOAchievementRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOAchievementRequest : LORESTRequest

+ (id)requestWithAchievementId:(NSUInteger)anId language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
