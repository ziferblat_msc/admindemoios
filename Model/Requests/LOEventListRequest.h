//
//  LOEventListRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;

@interface LOEventListRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)venueId language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
