//
//  LOCompleteActivityRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 27/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LOActivity;

@interface LOCompleteActivityRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue activity:(LOActivity *)anActivity completion:(LORESTRequestCompletion)aCompletion;

@end