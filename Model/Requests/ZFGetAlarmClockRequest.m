//
//  ZFGetAlarmClockRequest.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 13.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFGetAlarmClockRequest.h"

@implementation ZFGetAlarmClockRequest

#define kGetAlarmClockUrl   kBaseUrl    @"/alarm/single/%lu"

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kGetAlarmClockUrl, (unsigned long)alarmClockId];
    return [LORESTRequest getURL:url resultClass:[ZFAlarmClock class] completion:aCompletion];
}

@end
