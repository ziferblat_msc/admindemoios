//
//  LOSaveAchievementRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOAchievement;

@interface LOSaveAchievementRequest : LORESTRequest

+ (id)requestWithAchievement:(LOAchievement *)anAchievement language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
