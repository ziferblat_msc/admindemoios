//
//  LOAccountBalanceRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;

@interface LOAccountBalanceRequest : LORESTRequest

+ (id)requestWithMemberIdentifier:(NSNumber *)memberId completion:(LORESTRequestCompletion)aCompletion;
    
@end
