//
//  LOComparativeAnalysisRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOComparativeAnalysisRequest.h"
#import "LOStatsAnalysis.h"

@implementation LOComparativeAnalysisRequest

#define kComparativeAnalysisUrl  kBaseUrl @"stats/%ld/comparison?unit=%ld&value=%ld&dates=%@"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                             forDates:(NSArray *)datesArray
                           completion:(LORESTRequestCompletion)aCompletion {
    NSMutableString *dateString = [[NSMutableString alloc] initWithString:@""];
    for (NSString *dates in datesArray) {
        [dateString appendString:dates];
        if (![dates isEqualToString:[datesArray lastObject]]) {
            [dateString appendString:@","];
        }
    }
    
    NSString *url = [NSString stringWithFormat:kComparativeAnalysisUrl, (long)venueId, (long)unit, (long)time, dateString];
    return [LORESTRequest getURL:url resultClass:[LOStatsAnalysis class] completion:aCompletion];
}

@end
