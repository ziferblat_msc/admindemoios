//
//  LOAllAdminsListRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAllAdminsListRequest.h"
#import "LOMember.h"

@implementation LOAllAdminsListRequest

#define kAllAdminsUrl   kBaseUrl    @"member/admins"

+ (id)requestWithCompletion:(LORESTRequestCompletion)aCompletion
{
    return [LOAllAdminsListRequest getURL:kAllAdminsUrl resultClass:[LOMember class] completion:aCompletion];
}

@end
