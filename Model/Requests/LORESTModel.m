//
//  LORESTModel.m
//  Locassa
//
//  Created by  on 14/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import "LORESTModel.h"
#import <objc/runtime.h>

@implementation LORESTModel

- (id)init {
    self = [super init];
    
    if(self != nil) {
        REST_IGNORE_ENCODE(@"superclass");
        REST_IGNORE_ENCODE(@"description");
        REST_IGNORE_ENCODE(@"debugDescription");
    }
    
    return self;
}

- (void)ignoreDecodeKey:(NSString *)aKey {
    if(ignoreDecodeList == nil) {
        ignoreDecodeList = [[NSMutableSet alloc] init];
    }
    
    [ignoreDecodeList addObject:aKey];
}

- (void)ignoreEncodeKey:(NSString *)aKey {
    if(ignoreEncodeList == nil) {
        ignoreEncodeList = [[NSMutableSet alloc] init];
    }
    
    [ignoreEncodeList addObject:aKey];
}

- (void)addMappingFromKey:(NSString *)fromKey toKey:(NSString *)toKey {
    if(mappings == nil) {
        mappings = [[NSMutableDictionary alloc] init];
    }
    
    [mappings setObject:toKey forKey:fromKey];
}

- (void)addClassMappingForKey:(NSString *)aKey class:(Class)aClass
{
    if(classMappings == nil) {
        classMappings = [NSMutableDictionary dictionary];
    }

    [classMappings setObject:NSStringFromClass(aClass) forKey:aKey];
}

- (Class)classMappingForKey:(NSString *)aKey {
    NSString *className = [classMappings objectForKey:aKey];
    
    if(className != nil) {
        return NSClassFromString(className);
    }
    
    return nil;
}

- (void)setValue:(id)aValue forUndefinedKey:(NSString *)aKey {
    if([aValue isEqual:[NSNull null]]) {
        return;
    }
    
    NSString *mapping = [mappings objectForKey:aKey];
    
    if(mapping != nil) {
        [self setValue:aValue forKey:mapping];
        return;
    }
    
    if([ignoreDecodeList containsObject:aKey]) {
        return;
    }
    
#ifdef DEBUG
    NSLog(@"REST Model: %@ Unmapped property: %@", NSStringFromClass([self class]), aKey);
#endif
    
    // Enable this line if you want to force the local model
    // to be in sync with the server, in this case you will
    // get an exception for any unmapped properties
    //[super setValue:aValue forUndefinedKey:aKey];
}

- (NSMutableDictionary *)dictionary {
    return [self dictionaryFromClass:[self class]];
}

- (NSMutableDictionary *)dictionaryFromClass:(Class)aClass {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    unsigned int outCount, i;
    
    objc_property_t *properties = class_copyPropertyList(aClass, &outCount);
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
    
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        
        if([ignoreEncodeList containsObject:propertyName]) {
            continue;
        }

        id propertyValue = [self valueForKey:(NSString *)propertyName];
        NSString *mappedPropertyName = propertyName;
        
        for(NSString *key in mappings.allKeys) {
            if([[mappings objectForKey:key] isEqualToString:mappedPropertyName]) {
                mappedPropertyName = key;
            }
        }
        
        if(propertyValue) {
            if([propertyValue isKindOfClass:[NSDate class]]) {
                [data setObject:[outputFormatter stringFromDate:propertyValue] forKey:mappedPropertyName];
            } else {
                if([propertyValue isKindOfClass:[LORESTModel class]]) {
                    [data setObject:[propertyValue dictionary] forKey:mappedPropertyName];
                } else if ([propertyValue isKindOfClass:[NSArray class]]) {
                    if ([[propertyValue firstObject] isKindOfClass:[LORESTModel class]]) {
                        NSMutableArray *restModelArray = [NSMutableArray new];
                        for (id object in propertyValue) {
                            if ([object isKindOfClass:[LORESTModel class]]) {
                                [restModelArray addObject:[(LORESTModel *)object dictionary]];
                            }
                        }
                        [data setObject:restModelArray forKey:mappedPropertyName];
                    } else {
                        [data setObject:propertyValue forKey:mappedPropertyName];
                    }
                } else {
                    [data setObject:propertyValue forKey:mappedPropertyName];
                }
            }
        }
    }
    
    free(properties);
    Class superClass = class_getSuperclass(aClass);
    
    if((superClass != nil) && (![superClass isEqual:[NSObject class]])) {
        NSDictionary *otherData = [self dictionaryFromClass:superClass];
        [data addEntriesFromDictionary:otherData];
    }
    
    return data;
}

- (void)addValue:(LORESTModel *)value toData:(NSMutableDictionary *)someData forKey:(NSString *)aKey {
    id valueData = [value dictionary];
    
    if(valueData != nil) {
        [someData setObject:valueData forKey:aKey];
    }
}

- (void)addValues:(NSArray *)values toData:(NSMutableDictionary *)someData forKey:(NSString *)aKey {
    id valueData = [values valueForKeyPath:@"dictionary"];
    
    if(valueData != nil) {
        [someData setObject:valueData forKey:aKey];
    }
}

@end