//
//  LOActivityRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 30/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOActivityRequest.h"
#import "LOVenue.h"
#import "LOActivity.h"

@implementation LOActivityRequest

#define kActivityUrl  kBaseUrl @"venue/%@/activities/%ld/%@"

+ (id)requestWithVenue:(NSNumber *)venueIdentifier
            activityId:(NSInteger)activityId
              language:(NSString *)languageStr
            completion:(LORESTRequestCompletion)aCompletion {
    
    NSString *url = [NSString stringWithFormat: kActivityUrl, venueIdentifier.stringValue, (long)activityId, languageStr];
    return [LORESTRequest getURL:url resultClass:[LOActivity class] completion:aCompletion];
}

@end
