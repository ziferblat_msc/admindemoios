//
//  LOConfirmCheckOutMemberRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;
@class LOCheckOut;

@interface LOConfirmCheckOutMemberRequest : LORESTRequest

+ (id)requestWithMember:(LOMember *)aMember checkOut:(LOCheckOut *)aCheckOut completion:(LORESTRequestCompletion)aCompletion;

@end
