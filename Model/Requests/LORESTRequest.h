//
//  LORESTRequest.h
//  Locassa
//
//  Created by  on 14/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LORESTRequest;

typedef void (^LORESTRequestCompletion)(LORESTRequest *aRequest);

typedef NS_ENUM(NSInteger, LORESTRequestMethod) {
    LORESTRequestMethodGET,
    LORESTRequestMethodPOST,
    LORESTRequestMethodPUT,
    LORESTRequestMethodDELETE
};

typedef NS_ENUM(NSInteger, LORESTRequestResultStatus){
    LORESTRequestResultStatusPending,
    LORESTRequestResultStatusFailed,
    LORESTRequestResultStatusSucceeded,
    LORESTRequestResultStatusNotFound,
    LORESTRequestResultStatusConflict
};

@class LORESTModel;

@interface LORESTRequest : NSObject<NSURLConnectionDelegate> {
    NSURLConnection *connection;
    NSMutableData *receivedData;
    Class resultClass;
    NSUInteger statusCode;
    
    LORESTRequestCompletion completion;
    LORESTRequestResultStatus resultStatus;
    id result;
}

@property (nonatomic, readonly) NSMutableData *receivedData;
@property (nonatomic, readonly) Class resultClass;
@property (nonatomic, readonly) LORESTRequestResultStatus resultStatus;
@property (nonatomic, readonly) id result;
@property (nonatomic, weak) id target;
@property (nonatomic, strong)  NSString *localizedStatusCodeDescription;

+ (id)getURL:(NSString *)aURL resultClass:(Class)aClass completion:(LORESTRequestCompletion)aCompletion;
+ (id)getURL:(NSString *)aURL completion:(LORESTRequestCompletion)aCompletion;
+ (id)deleteURL:(NSString *)aURL completion:(LORESTRequestCompletion)aCompletion;
+ (id)deleteURL:(NSString *)aUrl resultClass:(Class)aClass completion:(LORESTRequestCompletion)aCompletion;
+ (id)postURL:(NSString *)aUrl resultClass:(Class)aClass data:(LORESTModel *)data completion:(LORESTRequestCompletion)aCompletion;
+ (id)postURL:(NSString *)aUrl resultClass:(Class)aClass array:(NSArray *)anArray completion:(LORESTRequestCompletion)aCompletion;
+ (id)putURL:(NSString *)aUrl resultClass:(Class)aClass data:(LORESTModel *)data completion:(LORESTRequestCompletion)aCompletion;

- (id)initWithURL:(NSString *)aUrl resultClass:(Class)aClass method:(LORESTRequestMethod)aMethod data:(NSData *)data completion:(LORESTRequestCompletion)aCompletion;

#pragma mark - Utils
+ (NSData *)encodeJsonData:(id)data;

@end
