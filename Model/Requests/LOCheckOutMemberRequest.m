//
//  LOCheckOutMemberRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOCheckOutMemberRequest.h"
#import "LOMember.h"
#import "LOCheckOut.h"

@implementation LOCheckOutMemberRequest

#define kCheckOutMemberUrl   kBaseUrl    @"member/%ld/venue/%ld/"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion {
    NSUInteger venueId = aMember.currentVenueId;
    NSString *url = [NSString stringWithFormat:kCheckOutMemberUrl, (long)aMember.memberId, (long)venueId];
    LORESTRequest *request = [LORESTRequest deleteURL:url resultClass:[LOCheckOut class] completion:aCompletion];
    return request;
}

@end
