//
//  LODeleteAchievementLevelRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODeleteAchievementLevelRequest.h"
#import "LOAchievementLevel.h"
#import "LOAchievement.h"

@implementation LODeleteAchievementLevelRequest

#define kDeleteAchievementLevelUrl   kBaseUrl    @"achievements/%ld/%ld"

+ (id)requestWithAchievementLevel:(LOAchievementLevel *)anAchievementLevel fromAchievement:(LOAchievement *)anAchievement
                       completion:aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteAchievementLevelUrl, (long)anAchievement.achievementId, (long)anAchievementLevel.achievementLevelId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
