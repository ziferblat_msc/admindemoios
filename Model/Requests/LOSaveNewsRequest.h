//
//  LOSaveNewsRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LONews;

@interface LOSaveNewsRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)venueId news:(LONews *)anNews language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
