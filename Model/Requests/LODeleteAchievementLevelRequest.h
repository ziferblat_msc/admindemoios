//
//  LODeleteAchievementLevelRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOAchievementLevel;
@class LOAchievement;

@interface LODeleteAchievementLevelRequest : LORESTRequest

+ (id)requestWithAchievementLevel:(LOAchievementLevel *)anAchievementLevel fromAchievement:(LOAchievement *)anAchievement completion:aCompletion;

@end
