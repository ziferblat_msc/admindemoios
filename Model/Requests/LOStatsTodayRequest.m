//
//  LOStatsTodayRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOStatsTodayRequest.h"
#import "LOStatsCurrent.h"

@implementation LOStatsTodayRequest

#define kStatsTodayUrl  kBaseUrl @"stats/%ld/today?sittingNow=%ld&includeSitting=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                           sittingNow:(BOOL)sittingNow
                       includeSitting:(BOOL)includeSitting
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion {
    NSMutableString *url = [NSMutableString stringWithFormat:kStatsTodayUrl, (long)venueId, (long)@(sittingNow).integerValue, (long)@(includeSitting).integerValue];
    if (memberTypes.count > 0) {
        [url appendFormat:@"&memberTypes=%@", [self buildStringForMemberTypes:memberTypes]];
    }
    return [LORESTRequest getURL:url resultClass:[LOStatsCurrent class] completion:aCompletion];
}

#pragma mark - Helpers

+ (NSString *)buildStringForMemberTypes:(NSArray *)memberTypes {
    
    NSMutableString *memberTypesString = [[NSMutableString alloc] initWithString:@""];
    for (NSNumber *memberType in memberTypes) {
        [memberTypesString appendString:[NSString stringWithFormat:@"%@", memberType]];
        if (![memberType isEqualToNumber:[memberTypes lastObject]]) {
            [memberTypesString appendString:@","];
        }
    }
    return memberTypesString;
}

@end
