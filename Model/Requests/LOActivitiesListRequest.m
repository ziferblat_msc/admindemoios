//
//  LOActivitiesListRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOActivitiesListRequest.h"
#import "LOVenue.h"
#import "LOActivity.h"

@implementation LOActivitiesListRequest

#define kActivityUrl  kBaseUrl @"venue/%@/activities/en/?page=%ld&count=%ld&sort=date&order=ASC"

+ (id)requestWithVenueIdentifier:(NSNumber *)venueNumber
                            page:(NSInteger)page
                       fetchSize:(NSInteger)fetchSize
                      completion:(LORESTRequestCompletion)aCompletion {
    
    NSString *url = [NSString stringWithFormat:kActivityUrl, venueNumber.stringValue, (long)page, (long)fetchSize];
    return [LORESTRequest getURL:url resultClass:[LOActivity class] completion:aCompletion];
}

@end
