//
//  LOCheckInMemberRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOCheckInMemberRequest.h"
#import "LOMember.h"

@implementation LOCheckInMemberRequest

#define kCheckInMemberUrl   kBaseUrl    @"member/%ld/venue/%ld/"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion {
    NSUInteger venueId = [[LOVenueService instance] assignedVenueId];
    NSString *url = [NSString stringWithFormat:kCheckInMemberUrl, (long)aMember.memberId, (long)venueId];
    LORESTRequest *request = [LORESTRequest postURL:url resultClass:[LOMember class] data:nil completion:aCompletion];
    return request;
}

@end
