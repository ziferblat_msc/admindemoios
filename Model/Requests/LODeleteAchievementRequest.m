//
//  LODeleteAchievementRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODeleteAchievementRequest.h"
#import "LOAchievement.h"

@implementation LODeleteAchievementRequest

#define kDeleteAchievementUrl   kBaseUrl    @"achievements/%ld"

+ (id)requestWithAchievement:(LOAchievement *)anAchievement completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteAchievementUrl, (long)anAchievement.achievementId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
