//
//  LOSocialStatsByVenueRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOSocialStatsByVenueRequest : LORESTRequest

+ (instancetype)requestStatsAtVenues:(NSArray *)venuesArray completion:(LORESTRequestCompletion)aCompletion;

@end
