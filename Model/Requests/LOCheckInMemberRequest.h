//
//  LOCheckInMemberRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;

@interface LOCheckInMemberRequest : LORESTRequest

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion;

@end
