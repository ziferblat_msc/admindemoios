//
//  LOLoginRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOLoginRequest : LORESTRequest

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

+ (id)requestWithUsername:(NSString *)aUsername password:(NSString *)aPassword completion:(LORESTRequestCompletion)aCompletion;

@end
