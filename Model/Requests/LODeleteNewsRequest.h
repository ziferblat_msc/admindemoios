//
//  LODeleteNewsRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LONews;

@interface LODeleteNewsRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue news:(LONews *)anNews completion:(LORESTRequestCompletion)aCompletion;

@end
