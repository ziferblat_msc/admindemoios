//
//  LOChangeAdminPasswordRequest.m
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOChangeAdminPasswordRequest.h"
#import "LOMember.h"
#import "LOChangingPassword.h"

@implementation LOChangeAdminPasswordRequest

#define kChangeAdminPasswordUrl   kBaseUrl    @"member/password/change"

+ (id)requestWithAdminMember:(LOMember *)adminMember oldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword completion:(LORESTRequestCompletion)aCompletion {
    
    LOChangingPassword *changingPassword = [LOChangingPassword new];
    changingPassword.oldPassword = oldPassword;
    changingPassword.passwordNew = newPassword;
    changingPassword.memberId = [NSNumber numberWithInteger:adminMember.memberId];
    
    LORESTRequest *request = [LORESTRequest postURL:kChangeAdminPasswordUrl resultClass:nil data:changingPassword completion:aCompletion];
    return request;
    
}

@end
