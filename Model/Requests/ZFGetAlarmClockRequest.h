//
//  ZFGetAlarmClockRequest.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 13.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "ZFAlarmClock.h"

@interface ZFGetAlarmClockRequest : LORESTRequest

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId completion:(LORESTRequestCompletion)aCompletion;

@end
