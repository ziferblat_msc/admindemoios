//
//  LOEventListRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEventListRequest.h"
#import "LOVenue.h"
#import "LOEvent.h"

@implementation LOEventListRequest

#define kEventsUrl  kBaseUrl @"venue/%ld/events/%@"

+ (id)requestWithVenueId:(NSUInteger)venueId language:(NSString *)languageStr  completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kEventsUrl, (long)venueId, languageStr];
    return [LORESTRequest getURL:url resultClass:[LOEvent class] completion:aCompletion];
}

@end
