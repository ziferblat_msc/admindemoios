//
//  LOAchievementRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievementRequest.h"
#import "LOAchievement.h"

@implementation LOAchievementRequest

#define kAchievementUrl   kBaseUrl    @"achievements/%ld/%@"

+ (id)requestWithAchievementId:(NSUInteger)anId language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kAchievementUrl, (long)anId, languageStr];
    return [LOAchievementRequest getURL:url resultClass:[LOAchievement class] completion:aCompletion];
}

@end
