//
//  LOSaveEventRequest.h
//  ZiferblatAdmin
//
//  Created by Administrator on 16/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LOEvent;

@interface LOSaveEventRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)venueId event:(LOEvent *)anEvent language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
