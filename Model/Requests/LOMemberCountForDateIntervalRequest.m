//
//  LOMemberCountForDateIntervalRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOMemberCountForDateIntervalRequest.h"

@implementation LOMemberCountForDateIntervalRequest

#define kMemberCountForDateIntervalUrl  kBaseUrl @"stats/%ld/interval/count?unit=%ld&value=%ld&includeSitting=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                       includeSitting:(BOOL)includeSitting
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion {
    NSMutableString *url = [NSMutableString stringWithFormat:kMemberCountForDateIntervalUrl, (long)venueId, (long)unit, (long)time, (long)@(includeSitting).integerValue];
    if (memberTypes.count > 0) {
        [url appendFormat:@"&memberTypes=%@", [self buildStringForMemberTypes:memberTypes]];
    }
    return [LORESTRequest getURL:url resultClass:nil completion:aCompletion];
}

#pragma mark - Helpers

+ (NSString *)buildStringForMemberTypes:(NSArray *)memberTypes {
    
    NSMutableString *memberTypesString = [[NSMutableString alloc] initWithString:@""];
    for (NSNumber *memberType in memberTypes) {
        [memberTypesString appendString:[NSString stringWithFormat:@"%@", memberType]];
        if (![memberType isEqualToNumber:[memberTypes lastObject]]) {
            [memberTypesString appendString:@","];
        }
    }
    return memberTypesString;
}

@end
