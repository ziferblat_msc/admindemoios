//
//  LOSaveEventRequest.m
//  ZiferblatAdmin
//
//  Created by Administrator on 16/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveVenueRequest.h"
#import "LOVenue.h"
#import "LOEvent.h"

@implementation LOSaveEventRequest

#define kUpdateEventsUrl   kBaseUrl    @"venue/%ld/event/%@"

+ (id)requestWithVenueId:(NSUInteger)venueId event:(LOEvent *)anEvent language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kUpdateEventsUrl, (unsigned long)venueId, languageStr];
    LORESTRequest *request = nil;
    
    if(anEvent.eventId == 0) {
        request = [LORESTRequest postURL:url resultClass:[LOEvent class] data:anEvent completion:aCompletion];
    } else {
        request = [LORESTRequest putURL:url resultClass:[LOEvent class] data:anEvent completion:aCompletion];
    }
    
    return request;
}

@end
