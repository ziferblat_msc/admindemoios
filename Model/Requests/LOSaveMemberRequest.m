//
//  LOSaveMemberRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveMemberRequest.h"
#import "LOMember.h"

@implementation LOSaveMemberRequest

#define kCreateMemberUrl   kBaseUrl    @"member"
#define kUpdateMemberUrl   kBaseUrl    @"member/%ld"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = nil;
    LORESTRequest *request = nil;
    
    if(aMember.memberId == 0)
    {
        url = kCreateMemberUrl;
        request = [LORESTRequest postURL:url resultClass:[LOMember class] data:aMember completion:aCompletion];
    }
    else
    {
        url = [NSString stringWithFormat:kUpdateMemberUrl, (unsigned long)aMember.memberId];
        request = [LORESTRequest putURL:url resultClass:[LOMember class] data:aMember completion:aCompletion];
    }
    
    return request;
}

@end
