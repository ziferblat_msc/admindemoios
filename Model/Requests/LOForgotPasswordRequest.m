//
//  LOForgotPasswordRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOForgotPasswordRequest.h"
#import "LOMember.h"

@implementation LOForgotPasswordRequest

#define kForgotPasswordUrl   kBaseUrl    @"member/password/forgot"

+ (id)requestWithEmail:(NSString *)email
            completion:(LORESTRequestCompletion)aCompletion {
    NSDictionary *dict = @{ @"email" : email };
    
    LORESTRequest *request = [[LOForgotPasswordRequest alloc] initWithURL:kForgotPasswordUrl resultClass:[LOMember class]
                                                                   method:LORESTRequestMethodPOST
                                                                     data:[LORESTRequest encodeJsonData:dict]
                                                               completion:aCompletion];
//    
    return request;
}

@end
