//
//  LOVenueListRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOVenueListRequest.h"
#import "LOVenue.h"
#import "LOMember.h"

@implementation LOVenueListRequest

#define kVenueListUrl       kBaseUrl    @"venue/en"
#define kAdminVenueListUrl  kBaseUrl    @"venue/admin/%ld/en"

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion {
    if([aMember.role isEqualToString:@"GLOBAL"])
    {
        LORESTRequest *request = [LORESTRequest getURL:kVenueListUrl resultClass:[LOVenue class] completion:aCompletion];
        return request;
    }
    
    NSString *url = [NSString stringWithFormat:kAdminVenueListUrl, (long)aMember.memberId];
    LORESTRequest *request = [LORESTRequest getURL:url resultClass:[LOVenue class] completion:aCompletion];
    return request;
}

@end
