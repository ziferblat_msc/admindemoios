//
//  LODeleteEventRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 19/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODeleteVenueRequest.h"
#import "LOVenue.h"
#import "LOEvent.h"

@implementation LODeleteEventRequest

#define kDeleteEventUrl   kBaseUrl    @"venue/%ld/event/%ld"

+ (id)requestWithVenue:(LOVenue *)aVenue event:(LOEvent *)anEvent completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteEventUrl, (long)aVenue.venueId, (long)anEvent.eventId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
