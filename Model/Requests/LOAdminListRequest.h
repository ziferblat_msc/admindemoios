//
//  LOGlobalAdminRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOAdminListRequest : LORESTRequest

+ (id)requestWithVenueId:(NSInteger)aVenueId completion:(LORESTRequestCompletion)aCompletion;

@end
