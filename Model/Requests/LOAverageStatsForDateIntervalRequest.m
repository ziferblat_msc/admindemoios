//
//  LOAverageStatsForDateIntervalRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOAverageStatsForDateIntervalRequest.h"
#import "LOStatsAverage.h"

@implementation LOAverageStatsForDateIntervalRequest

#define kAverageStatsForDateIntervalUrl  kBaseUrl @"stats/%ld/interval/average?unit=%ld&value=%ld&includeSitting=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kAverageStatsForDateIntervalUrl, (long)venueId, (long)unit, (long)time, (long)@(includeSitting).integerValue];
    return [LORESTRequest getURL:url resultClass:[LOStatsAverage class] completion:aCompletion];
}

@end
