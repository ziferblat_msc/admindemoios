//
//  LOSaveVenueRequest.h
//  ZiferblatAdmin
//
//  Created by Administrator on 16/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;

@interface LOSaveVenueRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
