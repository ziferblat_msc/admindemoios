//
//  LOSaveAchievementRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOSaveAchievementRequest.h"
#import "LOAchievement.h"

@implementation LOSaveAchievementRequest

#define kCreateAchievementUrl   kBaseUrl    @"achievements/%@"
#define kUpdateAchievementUrl   kBaseUrl    @"achievements/%ld/%@"

+ (id)requestWithAchievement:(LOAchievement *)anAchievement language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = nil;
    LORESTRequest *request = nil;
    
    if(anAchievement.achievementId == 0) {
        url = [NSString stringWithFormat:kCreateAchievementUrl, languageStr];
        request = [LORESTRequest postURL:url resultClass:[LOAchievement class] data:anAchievement completion:aCompletion];
    } else {
        url = [NSString stringWithFormat:kUpdateAchievementUrl, (unsigned long)anAchievement.achievementId, languageStr];
        request = [LORESTRequest putURL:url resultClass:[LOAchievement class] data:anAchievement completion:aCompletion];
    }
    
    return request;
}

@end
