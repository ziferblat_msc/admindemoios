//
//  LOPassword.h
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LOChangingPassword : LORESTModel

@property (strong, nonatomic) NSString *oldPassword;
@property (strong, nonatomic) NSString *passwordNew;
@property (strong, nonatomic) NSNumber *memberId;

@end
