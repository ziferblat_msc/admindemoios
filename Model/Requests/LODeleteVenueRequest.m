//
//  LODeleteVenueRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 19/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LODeleteVenueRequest.h"
#import "LOVenue.h"

@implementation LODeleteVenueRequest

#define kDeleteVenueUrl   kBaseUrl    @"venue/%ld"

+ (id)requestWithVenue:(LOVenue *)aVenue completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kDeleteVenueUrl, (long)aVenue.venueId];
    LORESTRequest *request = [LORESTRequest deleteURL:url completion:aCompletion];
    return request;
}

@end
