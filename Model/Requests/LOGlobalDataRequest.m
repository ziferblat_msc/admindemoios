//
//  LOGlobalDataRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOGlobalDataRequest.h"
#import "LOGlobalData.h"

@implementation LOGlobalDataRequest

#define kGlobalDataUrl  kBaseUrl @"global/"

+ (instancetype)requestWithCompletion:(LORESTRequestCompletion)aCompletion {
    return [LORESTRequest getURL:kGlobalDataUrl resultClass:[LOGlobalData class] completion:aCompletion];
}

@end
