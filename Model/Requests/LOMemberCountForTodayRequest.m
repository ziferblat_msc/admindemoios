//
//  LOMemberCountForTodayRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOMemberCountForTodayRequest.h"

@implementation LOMemberCountForTodayRequest

#define kMemberCountForDateIntervalUrl  kBaseUrl @"stats/%ld/today/count?sittingNow=%ld&includeSitting=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                           sittingNow:(BOOL)sittingNow
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [self buildUrlStringWithVenueId:venueId sittingNow:sittingNow includeSitting:includeSitting];
    return [LORESTRequest getURL:url resultClass:nil completion:aCompletion];
}

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                           sittingNow:(BOOL)sittingNow
                       includeSitting:(BOOL)includeSitting
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:@"%@&memberTypes=%@",
                     [self buildUrlStringWithVenueId:venueId sittingNow:sittingNow includeSitting:includeSitting],
                     [self buildStringForMemberTypes:memberTypes]];
    return [LORESTRequest getURL:url resultClass:nil completion:aCompletion];
}

#pragma mark - Helpers

+ (NSString *)buildUrlStringWithVenueId:(NSInteger)venueId
                             sittingNow:(BOOL)sittingNow
                         includeSitting:(BOOL)includeSitting {
    return [NSString stringWithFormat:kMemberCountForDateIntervalUrl, (long)@(sittingNow).integerValue, (long)time, (long)@(includeSitting).integerValue];
}

+ (NSString *)buildStringForMemberTypes:(NSArray *)memberTypes {
    
    NSMutableString *memberTypesString = [[NSMutableString alloc] initWithString:@""];
    for (NSNumber *memberType in memberTypes) {
        [memberTypesString appendString:[NSString stringWithFormat:@"%@", memberType]];
        if (![memberType isEqualToNumber:[memberTypes lastObject]]) {
            [memberTypesString appendString:@","];
        }
    }
    return memberTypesString;
}

@end
