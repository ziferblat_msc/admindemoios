//
//  LOSaveActivityRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOSaveActivityRequest.h"
#import "LOVenue.h"
#import "LOActivity.h"

@implementation LOSaveActivityRequest

#define kUpdateActivityUrl   kBaseUrl    @"venue/%ld/activity/%@"

+ (id)requestWithVenueId:(NSUInteger)venueId activity:(LOActivity *)anActivity language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kUpdateActivityUrl, (unsigned long)venueId, languageStr];
    LORESTRequest *request = nil;
    
    if(anActivity.activityId == 0) {
        request = [LORESTRequest postURL:url resultClass:[LOActivity class] data:anActivity completion:aCompletion];
    } else {
        request = [LORESTRequest putURL:url resultClass:[LOActivity class] data:anActivity completion:aCompletion];
    }
    
    return request;
}

@end
