//
//  ZFUploadImageRequest.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 28.03.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface ZFUploadImageRequest : LORESTRequest

+ (id)requestWithImage:(UIImage *)image filename:(NSString *)filename category:(NSString *)category completion:(LORESTRequestCompletion)aCompletion;

@end
