//
//  LOConfirmCheckOutMemberRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOConfirmCheckOutMemberRequest.h"
#import "LOMember.h"
#import "LOCheckOut.h"

@implementation LOConfirmCheckOutMemberRequest

#define kConfirmCheckOutMemberUrl   kBaseUrl    @"member/confirm/%ld/venue/%ld/en"

+ (id)requestWithMember:(LOMember *)aMember checkOut:(LOCheckOut *)aCheckOut completion:(LORESTRequestCompletion)aCompletion {
    NSUInteger venueId = aMember.currentVenueId;
    NSString *url = [NSString stringWithFormat:kConfirmCheckOutMemberUrl, (long)aMember.memberId, (long)venueId];
    LORESTRequest *request = [LORESTRequest putURL:url resultClass:[LOMember class] data:aCheckOut completion:aCompletion];
    return request;
}

@end
