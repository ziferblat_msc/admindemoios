//
//  ZFSignInRequest.h
//  Ziferblat
//
//  Created by Terry Michel on 05/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface ZFAdminSignupRequest : LORESTRequest

+ (id)requestWithEmail:(NSString *)email
              password:(NSString *)password
            secretWord:(NSString *)secretWord
            completion:(LORESTRequestCompletion)aCompletion;

@end
