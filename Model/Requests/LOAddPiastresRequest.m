//
//  LOUpdateAccountBalanceRequest.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAddPiastresRequest.h"
#import "LOAccountBalance.h"
#import "LOMember.h"

@implementation LOAddPiastresRequest

#define kUpdateAccountBalanceUrl   kBaseUrl    @"balance/piastres/add/%ld?amount=%ld"

+ (id)requestWithMember:(LOMember *)aMember amount:(NSUInteger)anAmount completion:(LORESTRequestCompletion)aCompletion {
    NSString *url = [NSString stringWithFormat:kUpdateAccountBalanceUrl, (long)aMember.memberId, (long)anAmount];
    LORESTRequest *request = [LORESTRequest postURL:url resultClass:[LOAccountBalance class] data:nil completion:aCompletion];
    return request;
}

@end
