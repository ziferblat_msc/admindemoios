//
//  LONewsListRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LONewsListRequest.h"
#import "LOVenue.h"
#import "LONews.h"

@implementation LONewsListRequest

#define kNewsUrl  kBaseUrl @"venue/%lu/news/%@"

+ (id)requestWithVenueId:(NSUInteger)venueId language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion
{
    NSString *url = [NSString stringWithFormat:kNewsUrl, (unsigned long)venueId, languageStr];
    return [LORESTRequest getURL:url resultClass:[LONews class] completion:aCompletion];
}

@end
