//
//  LOCompleteActivityRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 27/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOCompleteActivityRequest.h"
#import "LOVenue.h"
#import "LOActivity.h"

@implementation LOCompleteActivityRequest

#define kCompleteActivityUrl   kBaseUrl    @"venue/%ld/activities/%ld/complete/en"

+ (id)requestWithVenue:(LOVenue *)aVenue
              activity:(LOActivity *)anActivity
            completion:(LORESTRequestCompletion)aCompletion {
    
    NSString *url = [NSString stringWithFormat:kCompleteActivityUrl, (long)aVenue.venueId, (long)anActivity.activityId];
    LORESTRequest *request = [LORESTRequest postURL:url resultClass:nil data:anActivity completion:aCompletion];
    return request;
}

@end
