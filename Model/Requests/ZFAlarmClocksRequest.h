//
//  ZFAlarmClocksRequest.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface ZFAlarmClocksRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)venueId completion:(LORESTRequestCompletion)aCompletion;

@end
