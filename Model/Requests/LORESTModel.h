//
//  LORESTModel.h
//  Locassa
//
//  Created by  on 14/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LORESTModel : NSObject {
    NSMutableSet *ignoreDecodeList;
    NSMutableSet *ignoreEncodeList;
    NSMutableDictionary *mappings;
    NSMutableDictionary *classMappings;
}

#define REST_IGNORE_DECODE(aKey)            [self ignoreDecodeKey:aKey]
#define REST_IGNORE_ENCODE(aKey)            [self ignoreEncodeKey:aKey]
#define REST_MAP(sourceKey, destinationKey) [self addMappingFromKey:sourceKey toKey:destinationKey]
#define REST_CLASS(aKey, aClass)            [self addClassMappingForKey:aKey class:aClass]

- (void)ignoreDecodeKey:(NSString *)aKey;
- (void)ignoreEncodeKey:(NSString *)aKey;
- (void)addMappingFromKey:(NSString *)fromKey toKey:(NSString *)toKey;
- (void)addClassMappingForKey:(NSString *)aKey class:(Class)aClass;
- (Class)classMappingForKey:(NSString *)aKey;
- (NSMutableDictionary *)dictionary;
- (void)addValue:(LORESTModel *)value toData:(NSMutableDictionary *)someData forKey:(NSString *)aKey;
- (void)addValues:(NSArray *)values toData:(NSMutableDictionary *)someData forKey:(NSString *)aKey;

@end
