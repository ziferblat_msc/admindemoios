//
//  ZFSaveAlarmClockRequest.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "ZFAlarmClock.h"

@interface ZFSaveAlarmClockRequest : LORESTRequest


+ (id)requestWithVenueId:(NSUInteger)venueId alarmClock:(ZFAlarmClock *)anAlarmClock completion:(LORESTRequestCompletion)aCompletion;

@end
