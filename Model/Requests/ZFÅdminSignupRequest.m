//
//  ZFSignInRequest.m
//  Ziferblat
//
//  Created by Terry Michel on 05/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFAdminSignupRequest.h"
#import "LOMember.h"

@implementation ZFAdminSignupRequest

#define kAdminSignupUrl kBaseUrl @"member/signup?email=%@&password=%@&secret=%@"

+ (id)requestWithEmail:(NSString *)email
              password:(NSString *)password
            secretWord:(NSString *)secretWord
            completion:(LORESTRequestCompletion)aCompletion {
    NSString *urlString = [NSString stringWithFormat:kAdminSignupUrl, email, password, secretWord];
    ZFAdminSignupRequest *request = [[ZFAdminSignupRequest alloc] initWithURL:urlString
                                                                  resultClass:[LOMember class]
                                                                       method:LORESTRequestMethodGET
                                                                         data:nil
                                                                   completion:aCompletion];
    return request;
}

@end
