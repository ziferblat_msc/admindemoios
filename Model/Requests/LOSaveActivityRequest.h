//
//  LOSaveActivityRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LOActivity;

@interface LOSaveActivityRequest : LORESTRequest

+ (id)requestWithVenueId:(NSUInteger)venueId activity:(LOActivity *)anActivity language:(NSString *)languageStr completion:(LORESTRequestCompletion)aCompletion;

@end
