//
//  LODeleteMemberRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 17/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOMember;

@interface LODeleteMemberRequest : LORESTRequest

+ (id)requestWithMember:(LOMember *)aMember completion:(LORESTRequestCompletion)aCompletion;

@end
