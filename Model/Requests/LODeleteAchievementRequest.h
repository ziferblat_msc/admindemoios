//
//  LODeleteAchievementRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOAchievement;

@interface LODeleteAchievementRequest : LORESTRequest

+ (id)requestWithAchievement:(LOAchievement *)anAchievement completion:(LORESTRequestCompletion)aCompletion;

@end
