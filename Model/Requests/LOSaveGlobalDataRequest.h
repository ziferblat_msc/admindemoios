//
//  LOSaveGlobalDataRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "LOGlobalData.h"

@interface LOSaveGlobalDataRequest : LORESTRequest

+ (instancetype)requestWithGlobalData:(LOGlobalData *)someData completion:(LORESTRequestCompletion)aCompletion;

@end
