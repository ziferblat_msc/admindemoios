//
//  LOSaveAchievementLevelRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOAchievementLevel;
@class LOAchievement;

@interface LOSaveAchievementLevelRequest : LORESTRequest

+ (id)requestWithAchievementLevel:(LOAchievementLevel *)anAchievementLevel forAchievement:(LOAchievement *)anAchievement completion:aCompletion;

@end
