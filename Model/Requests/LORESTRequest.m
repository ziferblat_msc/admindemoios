//
//  LORESTService.m
//  Locassa
//
//  Created by  on 14/08/2012.
//  Copyright (c) 2012 Locassa Ltd. All rights reserved.
//

#import "LORESTRequest.h"
#import "LORESTModel.h"
#import "LORESTRequestService.h"

@implementation LORESTRequest

@synthesize resultClass;
@synthesize resultStatus;
@synthesize receivedData;
@synthesize result;
@synthesize target;

#define kRequestTimeout 30

+ (id)getURL:(NSString *)aUrl completion:(LORESTRequestCompletion)aCompletion {
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:nil method:LORESTRequestMethodGET data:nil completion:aCompletion];
}

+ (id)getURL:(NSString *)aUrl resultClass:(Class)aClass completion:(LORESTRequestCompletion)aCompletion {
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:aClass method:LORESTRequestMethodGET data:nil completion:aCompletion];
}

+ (id)deleteURL:(NSString *)aUrl completion:(LORESTRequestCompletion)aCompletion{
    return [self deleteURL:aUrl resultClass:nil completion:aCompletion];
}

+ (id)deleteURL:(NSString *)aUrl resultClass:(Class)aClass completion:(LORESTRequestCompletion)aCompletion {
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:aClass method:LORESTRequestMethodDELETE data:nil completion:aCompletion];
}

+ (id)postURL:(NSString *)aUrl resultClass:(Class)aClass data:(LORESTModel *)data completion:(LORESTRequestCompletion)aCompletion {
    NSData *json = data == nil ? nil : [NSJSONSerialization dataWithJSONObject:[data dictionary] options:0 error:nil];
    NSLog(@"JSON = %@",[[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:aClass method:LORESTRequestMethodPOST data:json completion:aCompletion];
}

+ (id)postURL:(NSString *)aUrl resultClass:(Class)aClass array:(NSArray *)anArray completion:(LORESTRequestCompletion)aCompletion
{
    NSMutableArray *jsonArray = [[NSMutableArray alloc] init];
    
    for (LORESTModel *object in anArray)
    {
        [jsonArray addObject:[object dictionary]];
    }
    
    NSData *json = jsonArray == nil ? nil : [NSJSONSerialization dataWithJSONObject:jsonArray options:0 error:nil];
    //NSLog(@"JSON = %@",[[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:aClass method:LORESTRequestMethodPOST data:json completion:aCompletion];
}

+ (id)putURL:(NSString *)aUrl resultClass:(Class)aClass data:(LORESTModel *)data completion:(LORESTRequestCompletion)aCompletion
{
    NSData *json = data == nil ? nil : [NSJSONSerialization dataWithJSONObject:[data dictionary] options:0 error:nil];
    NSLog(@"JSON = %@",[[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
    return [[LORESTRequest alloc] initWithURL:aUrl resultClass:aClass method:LORESTRequestMethodPUT data:json completion:aCompletion];
}

- (NSString *)httpMethod:(LORESTRequestMethod)aMethod
{
    switch(aMethod) {
        case LORESTRequestMethodGET:
            return @"GET";
            
        case LORESTRequestMethodPOST:
            return @"POST";
            
        case LORESTRequestMethodPUT:
            return @"PUT";

        case LORESTRequestMethodDELETE:
            return @"DELETE";
    }
}

- (id)initWithURL:(NSString *)aUrl resultClass:(Class)aClass method:(LORESTRequestMethod)aMethod data:(NSData *)data completion:(LORESTRequestCompletion)aCompletion {
    self = [super init];
    
    if(self != nil)
    {
        completion = aCompletion;
        [LORESTRequestService addRequest:self];
        resultStatus = LORESTRequestResultStatusPending;
        
        resultClass = aClass;        
        receivedData = [[NSMutableData alloc] init];
        
        NSString *encoded =  [aUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSURL *url = [NSURL URLWithString:encoded];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:kRequestTimeout];

        [request setHTTPMethod:[self httpMethod:aMethod]];
        
        if(data != nil) {
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:data];            
        }

        NSLog(@"%@", request.URL.absoluteString);
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    return self;
}

- (id)start {
    [connection start];
    return self;
}

#pragma mark -
#pragma mark Connection Delegate Methods

- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)response {
    if ([response respondsToSelector:@selector(statusCode)])
    {
        statusCode = [(NSHTTPURLResponse *)response statusCode];
        
        if(statusCode == 200) {
            return;
        }
        
        if(statusCode == 404) {
            resultStatus = LORESTRequestResultStatusNotFound;
        } else if(statusCode == 409) {
            resultStatus = LORESTRequestResultStatusConflict;
        } else {
            resultStatus = LORESTRequestResultStatusFailed;
        }
        
        self.localizedStatusCodeDescription = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
        
        NSLog(@"Received status: %lu", (unsigned long)statusCode);
        
        if(completion != nil) {
            completion(self);
        }
    }    
}

- (void)connection:(NSURLConnection *)aConnection didFailWithError:(NSError *)error
{
    NSLog(@"Request failed - %@", [error localizedDescription]);
    
    resultStatus = LORESTRequestResultStatusFailed;

    if(completion != nil) {
        completion(self);
    }

    [LORESTRequestService requestDidComplete:self];
}

- (void)connection:(NSURLConnection *)aConnection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if([challenge previousFailureCount] > 0) {
        [LORESTRequestService instance].username = nil;
        [LORESTRequestService instance].password = nil;
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
    
    NSString *username = [LORESTRequestService instance].username;
    NSString *password = [LORESTRequestService instance].password;
    
    if(username == nil) {
        return;
    }
    
    NSURLCredential *credential = [NSURLCredential credentialWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge]; 
}

- (void)connection:(NSURLConnection *)aConnection didReceiveData:(NSData *)someData
{
    NSLog(@"RESPONSE DATA = %@", [[NSString alloc] initWithData:someData encoding:NSUTF8StringEncoding]);
    [receivedData appendData:someData];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)aConnection {
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    if(resultStatus != LORESTRequestResultStatusPending) {
        return;
    }
    
    NSError *error = nil;
    NSObject *resultObject = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];

    if([resultObject isKindOfClass:[NSDictionary class]] && ([(NSDictionary *)resultObject objectForKey:@"content"] != nil)) {
        resultObject = [(NSDictionary *)resultObject objectForKey:@"content"];
    }
    
    if((resultObject != nil) && (error == nil)) {
        if([resultObject isKindOfClass:[NSDictionary class]]) {
            [self parseObject:resultObject];
        } else if([resultObject isKindOfClass:[NSArray class]]) {
            [self parseArray:resultObject];
        }
    } else {
        resultStatus = LORESTRequestResultStatusSucceeded;
        result = [response stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        if(completion != nil) {
            completion(self);
        }
    }
    
    [LORESTRequestService requestDidComplete:self];
}

#pragma mark -
#pragma mark Response Handling Methods

- (NSDictionary *)cleanData:(NSDictionary *)dictionary {
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    NSMutableArray *removeKeys = [[NSMutableArray alloc] init];
    
    for(NSString *key in data.allKeys) {
        if([[data objectForKey:key] isEqual:[NSNull null]]) {
            [removeKeys addObject:key];
        }
    }
    
    for(NSString *key in removeKeys) {
        [data removeObjectForKey:key];
    }
    
    return data;
}

- (void)parseObject:(NSObject *)resultObject {
    if(resultClass == nil) {
        resultStatus = LORESTRequestResultStatusSucceeded;
        
        if(completion != nil) {
            completion(self);
        }

        return;
    }
    
    LORESTModel *object = [self parseObject:resultObject class:resultClass];
    resultStatus = LORESTRequestResultStatusSucceeded;
    result = object;
    
    if(completion != nil) {
        completion(self);
    }
}

- (LORESTModel *)parseObject:(NSObject *)resultObject class:(Class)aClass {
    NSDictionary *itemData = nil;
    LORESTModel *object = [[aClass alloc] init];
    itemData = [self cleanData:(NSDictionary *)resultObject];
    
    for(NSString *key in itemData.allKeys) {
        id value = [itemData objectForKey:key];
        
        if(![value isEqual:[NSNull null]]) {
            if([value isKindOfClass:[NSArray class]]) {
                Class class = [object classMappingForKey:key];
                
                if(class != nil) {
                    NSMutableArray *parsedArray = [NSMutableArray array];
                    
                    for(NSDictionary *item in (NSArray *)value) {
                        [parsedArray addObject:[self parseObject:(NSDictionary *)item class:class]];
                    }
                    
                    [object setValue:parsedArray forKey:key];
                } else {
                    [object setValue:value forKey:key];
                }
            } else if([value isKindOfClass:[NSDictionary class]]) {
                Class class = [object classMappingForKey:key];
                
                if(class != nil) {
                    [object setValue:[self parseObject:(NSDictionary *)value class:class] forKey:key];
                } else {
                    [object setValue:value forKey:key];
                }
            } else {
                [object setValue:value forKey:key];
            }
        }
    }
    
    return object;
}

- (void)parseArray:(NSObject *)resultObject {
    if(resultClass == nil) {
        resultStatus = LORESTRequestResultStatusSucceeded;

        if(completion != nil) {
            completion(self);
        }

        return;
    }
    
    NSDictionary *itemData = nil;
    NSMutableArray *array = [NSMutableArray array];
    NSArray *resultData = (NSArray *)resultObject;
    NSEnumerator *resultsEnum = [resultData objectEnumerator];
    
    while (itemData = [resultsEnum nextObject]) {
        if([itemData isKindOfClass:[NSString class]]) {
            [array addObject:itemData];
            continue;
        }
        
        NSObject *object = [[resultClass alloc] init];
        itemData = [self cleanData:(NSDictionary *)itemData];
        
        for(NSString *key in itemData.allKeys) {
            [object setValue:[itemData objectForKey:key] forKey:key];
        }
        
        [array addObject:object];
    }
    
    resultStatus = LORESTRequestResultStatusSucceeded;
    result = array;

    if(completion != nil) {
        completion(self);
    }
}

#pragma mark - Utils
+ (NSData *)encodeJsonData:(id)data
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:0
                                                         error:&error];
    if (error)
    {
        NSLog(@"%@", error.localizedDescription);
    }
    return jsonData;
}

@end
