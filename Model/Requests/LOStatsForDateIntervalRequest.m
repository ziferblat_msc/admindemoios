//
//  LOStatsForDateIntervalRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOStatsForDateIntervalRequest.h"
#import "LOStatsCurrent.h"

@implementation LOStatsForDateIntervalRequest

#define kStatsForDateIntervalUrl  kBaseUrl @"stats/%ld/interval?unit=%ld&value=%ld"

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                           completion:(LORESTRequestCompletion)aCompletion {
    return [LOStatsForDateIntervalRequest requestWithUrl:[LOStatsForDateIntervalRequest buildUrlStringWithVenueId:venueId unitType:unit time:time]
                                              completion:aCompletion];
}

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *baseUrl = [LOStatsForDateIntervalRequest buildUrlStringWithVenueId:venueId unitType:unit time:time];
    NSString *url = [NSString stringWithFormat:@"%@&memberType=%@", baseUrl, [self buildStringForMemberTypes:memberTypes]];
    return [LOStatsForDateIntervalRequest requestWithUrl:url
                                              completion:aCompletion];
}

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *baseUrl = [LOStatsForDateIntervalRequest buildUrlStringWithVenueId:venueId unitType:unit time:time];
    NSString *url = [NSString stringWithFormat:@"%@&includeSitting=%ld", baseUrl, (long)@(includeSitting).integerValue];
    return [LOStatsForDateIntervalRequest requestWithUrl:url
                                              completion:aCompletion];
}

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                          memberTypes:(NSArray *)memberTypes
                       includeSitting:(BOOL)includeSitting
                           completion:(LORESTRequestCompletion)aCompletion {
    NSString *baseUrl = [LOStatsForDateIntervalRequest buildUrlStringWithVenueId:venueId unitType:unit time:time];
    NSString *url = [NSString stringWithFormat:@"%@&memberType=%@&includeSitting=%ld", baseUrl, [self buildStringForMemberTypes:memberTypes], (long)@(includeSitting).integerValue];
    return [LOStatsForDateIntervalRequest requestWithUrl:url
                                              completion:aCompletion];
}

+ (instancetype)requestWithUrl:(NSString *)url
                    completion:(LORESTRequestCompletion)aCompletion  {
    return [LORESTRequest getURL:url
                     resultClass:[LOStatsCurrent class]
                      completion:aCompletion];
}

+ (NSString *)buildUrlStringWithVenueId:(NSInteger)venueId
                               unitType:(UnitType)unit
                                   time:(NSInteger)time {
    return [NSString stringWithFormat:kStatsForDateIntervalUrl, (long)venueId, (long)unit, (long)time];
}

+ (NSString *)buildStringForMemberTypes:(NSArray *)memberTypes {
    
    NSMutableString *memberTypesString = [[NSMutableString alloc] initWithString:@""];
    for (NSNumber *memberType in memberTypes) {
        [memberTypesString appendString:[NSString stringWithFormat:@"%@", memberType]];
        if (![memberType isEqualToNumber:[memberTypes lastObject]]) {
            [memberTypesString appendString:@","];
        }
    }
    return memberTypesString;
}

@end
