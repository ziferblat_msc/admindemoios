//
//  LOSocialRequestConstants.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

typedef NS_ENUM(NSInteger, UnitType) {
    UnitTypeDay = 0,
    UnitTypeMonth = 1,
    UnitTypeYear = 2
};