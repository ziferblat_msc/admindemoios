//
//  LOResetPasswordRequest.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOResetPasswordRequest.h"
#import "LOMember.h"

@implementation LOResetPasswordRequest

#define kResetPasswordUrl   kBaseUrl    @"member/password/reset"

+ (id)requestWithEmail:(NSString *)email
            secretWord:(NSString *)secretWord
              password:(NSString *)password
            completion:(LORESTRequestCompletion)aCompletion {
    
    NSDictionary *dict = @{ @"email" : email,
                            @"secretWord" : secretWord,
                            @"password" : password };
    
    LOResetPasswordRequest *request = [[LOResetPasswordRequest alloc] initWithURL:kResetPasswordUrl
                                                                      resultClass:[LOMember class]
                                                                           method:LORESTRequestMethodPOST
                                                                             data:[LORESTRequest encodeJsonData:dict]
                                                                       completion:aCompletion];
    return request;
}

@end
