//
//  LOMemberCountForDateIntervalRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 03/11/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "LOSocialRequestConstants.h"

@interface LOMemberCountForDateIntervalRequest : LORESTRequest

+ (instancetype)requestStatsAtVenueId:(NSInteger)venueId
                             unitType:(UnitType)unit
                                 time:(NSInteger)time
                       includeSitting:(BOOL)includeSitting
                          memberTypes:(NSArray *)memberTypes
                           completion:(LORESTRequestCompletion)aCompletion;

@end
