//
//  ZFDeleteAlarmClock.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 12.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTRequest.h"
#import "ZFAlarmClock.h"

@interface ZFDeleteAlarmClock : LORESTRequest

+ (id)requestWithAlarmClockId:(NSUInteger)alarmClockId completion:(LORESTRequestCompletion)aCompletion;

@end
