//
//  LODeleteActivityRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;
@class LOActivity;

@interface LODeleteActivityRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue activity:(LOActivity *)anActivity completion:(LORESTRequestCompletion)aCompletion;

@end
