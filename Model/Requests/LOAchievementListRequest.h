//
//  LOAchievementListRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOAchievementListRequest : LORESTRequest

+ (id)requestWithCompletion:(LORESTRequestCompletion)aCompletion;

@end
