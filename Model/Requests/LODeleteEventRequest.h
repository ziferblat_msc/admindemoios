//
//  LODeleteEventRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 19/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOEvent;

@interface LODeleteEventRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue event:(LOEvent *)anEvent completion:(LORESTRequestCompletion)aCompletion;

@end
