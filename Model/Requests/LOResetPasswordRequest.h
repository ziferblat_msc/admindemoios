//
//  LOResetPasswordRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOResetPasswordRequest : LORESTRequest

+ (id)requestWithEmail:(NSString *)email
            secretWord:(NSString *)secretWord
              password:(NSString *)password
            completion:(LORESTRequestCompletion)aCompletion;

@end
