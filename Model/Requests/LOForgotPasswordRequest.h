//
//  LOForgotPasswordRequest.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@interface LOForgotPasswordRequest : LORESTRequest

+ (id)requestWithEmail:(NSString *)email
            completion:(LORESTRequestCompletion)aCompletion;

@end
