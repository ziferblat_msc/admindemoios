//
//  LOPassword.m
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 20/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOChangingPassword.h"

@implementation LOChangingPassword

- (instancetype)init {
    self = [super init];
    
    if(self != nil) {
        REST_MAP(@"newPassword", @"passwordNew");
    }
    
    return self;
}

@end
