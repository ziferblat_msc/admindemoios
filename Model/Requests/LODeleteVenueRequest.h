//
//  LODeleteVenueRequest.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 19/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTRequest.h"

@class LOVenue;

@interface LODeleteVenueRequest : LORESTRequest

+ (id)requestWithVenue:(LOVenue *)aVenue completion:(LORESTRequestCompletion)aCompletion;

@end
