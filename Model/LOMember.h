//
//  LOMember.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

typedef NS_ENUM(NSInteger, MemberGenderType) {
    MemberGenderTypeUnspecified = 0,
    MemberGenderTypeMale = 1,
    MemberGenderTypeFemale = 2
};

typedef NS_ENUM(NSInteger, MemberRole) {
    MemberRoleLocal = 0,
    MemberRoleAdmin = 1,
    MemberRoleGlobal = 2
};

#define kMemberGenderMale       NSLocalizedString(@"Male", nil)
#define kMemberGenderFemale     NSLocalizedString(@"Female", nil)

typedef NS_ENUM(NSInteger, MemberAgeRange) {
    MemberAgeRangeUnspecified = 0,
    MemberAgeRangeFourteenToEighteen = 1,
    MemberAgeRangeNineteenToTwentyFive = 2,
    MemberAgeRangeTwentySixToThirtyFive = 3,
    MemberAgeRangeThirtyFivePlus = 4
};

#define kMemberAgeRangeFourteenToEighteen       NSLocalizedString(@"14 to 18", nil)
#define kMemberAgeRangeNineteenToTwentyFive     NSLocalizedString(@"19 to 25", nil)
#define kMemberAgeRangeTwentySixToThirtyFive    NSLocalizedString(@"26 to 35", nil)
#define kMemberAgeRangeThirtyFivePlus           NSLocalizedString(@"36+", nil)

typedef NS_ENUM(NSInteger, MemberSocialStatusType)
{
    MemberSocialStatusTypeUnspecified = 0,
    MemberSocialStatusTypeSchoolStudent = 1,
    MemberSocialStatusTypeUniversityStudent = 2,
    MemberSocialStatusTypeCreativeJob = 3,
    MemberSocialStatusTypeFreelancer = 4,
    MemberSocialStatusTypeUnemployed = 5,
    MemberSocialStatusTypeEmployed = 6,
    MemberSocialStatusTypeEntrepreneur = 7,
    MemberSocialStatusTypeRetired = 8
};

#define kMemberSocialSchoolStudent          NSLocalizedString(@"School student", nil)
#define kMemberSocialUniversityStudent      NSLocalizedString(@"University student", nil)
#define kMemberSocialCreativeJob            NSLocalizedString(@"Creative", nil)
#define kMemberSocialFreelancer             NSLocalizedString(@"Freelancer", nil)
#define kMemberSocialUnemployed             NSLocalizedString(@"Unemployed", nil)
#define kMemberSocialEmployed               NSLocalizedString(@"Employed", nil)
#define kMemberSocialEntrepreneur           NSLocalizedString(@"Entrepreneur", nil)
#define kMemberSocialRetired                NSLocalizedString(@"Retired", nil)

@interface LOMember : LORESTModel

@property (nonatomic) NSInteger memberId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *secretWord;
@property (nonatomic, strong) NSString *role;
@property (nonatomic) NSInteger currentVenueId;
@property (nonatomic) NSInteger favouriteVenueId;
@property (nonatomic) MemberGenderType gender;
@property (nonatomic) MemberAgeRange age;
@property (nonatomic) MemberSocialStatusType socialStatus;
@property (nonatomic, strong) NSString *memberActivity;
@property (nonatomic, strong) NSArray *adminVenueIds;

@property (nonatomic, readonly) NSString *fullName;
@property (assign, readonly) bool isCheckedIn;
@property (assign) bool admin;
@property (assign) bool showInVenue;
@property (assign) bool showNameOnly;

#pragma mark - Helpers

+ (MemberGenderType)genderForTypeSelected:(NSString *)gender;
+ (NSString *)stringForGender:(MemberGenderType)genderType;
+ (MemberAgeRange)ageRangeForRangeSelected:(NSString *)range;
+ (NSString *)stringForAgeRange:(MemberAgeRange)ageRange;
+ (MemberSocialStatusType)socialStatusForStatusSelected:(NSString *)status;
+ (NSString *)stringForSocialStatus:(MemberSocialStatusType)socialStatusType;

@end
