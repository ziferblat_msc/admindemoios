//
//  LOAchievement.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 03/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"
#import "LOEditableItem.h"

@interface LOAchievement : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic) NSInteger achievementId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSArray<LOAchievementLevel *>*levels;
@property (nonatomic, strong) NSDate *lastUpdated;
@property (nonatomic, strong) NSNumber *dateCreated;

- (void)levelSaved:(LOAchievementLevel *)aLevel;
- (void)levelDeleted:(LOAchievementLevel *)aLevel;

- (void)validateAchievement:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;

@end
