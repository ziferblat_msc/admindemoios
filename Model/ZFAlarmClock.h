//
//  ZFAlarmClock.h
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface ZFAlarmClock : LORESTModel <NSCopying>

@property (nonatomic) NSUInteger id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSArray *members;//временно

@property (nonatomic, strong) LOVenue *venue;

@end
