//
//  LOVenue.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOVenue.h"

@implementation LOVenue

@synthesize venueId;
@synthesize name;
@synthesize address;
@synthesize currencyCode;
@synthesize stopCheck;
@synthesize detail;
@synthesize openingHours;
@synthesize telephone;
@synthesize facebook;
@synthesize twitter;
@synthesize instagram;
@synthesize vkontakte;
@synthesize website;
@synthesize latitude;
@synthesize longitude;
@synthesize piastresFactor;
@synthesize baseMinutesFactor;
@synthesize experienceFactor;
@synthesize imageUrls;
@synthesize lastUpdated;
@synthesize unlimitedSubscriptionCost;
@synthesize workingSubscriptionCost;
@synthesize workingSubscriptionStartTime;
@synthesize workingSubscriptionEndTime;

- (id)init {
    self = [super init];
    
    if(self != nil) {
        REST_IGNORE_DECODE(@"teamMemberCount");
        REST_IGNORE_DECODE(@"memberCount");
        REST_IGNORE_DECODE(@"occupantCount");
        [super addMappingFromKey:@"id" toKey:@"venueId"];
        [super addMappingFromKey:@"description" toKey:@"detail"];
        [super ignoreEncodeKey:@"venueId"];
        [super ignoreEncodeKey:@"lastUpdated"];
    }
    
    return self;
}

- (id)valueForKey:(NSString *)key {
    if([key isEqualToString:@"workingSubscriptionStartTime"]) {
        return @(workingSubscriptionStartTime.timeIntervalSince1970*1000);
    } else if([key isEqualToString:@"workingSubscriptionEndTime"]) {
        return @(workingSubscriptionEndTime.timeIntervalSince1970*1000);
    } else {
        return [super valueForKey:key];
    }
}

- (void)setWorkingSubscriptionStartTime:(NSObject *)aValue {
    NSDate *date = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        date = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        date = (NSDate *)aValue;
    }
    
    workingSubscriptionStartTime = date;
}

- (void)setWorkingSubscriptionEndTime:(NSObject *)aValue {
    NSDate *date = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        date = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        date = (NSDate *)aValue;
    }
    
    workingSubscriptionEndTime = date;
}

- (void)setLastUpdated:(NSObject *)aValue {
    NSDate *date = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        date = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        date = (NSDate *)aValue;
    }
    
    lastUpdated = date;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nvenue Id: %ld\nname: %@\ndetail: %@\naddress: %@\ncurrencyCode: %@", (long)venueId, name, detail, address, currencyCode];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    if (copy) {
        [copy setName:self.name];
        [copy setVenueId:self.venueId];
        [copy setDetail:self.detail];
        [copy setAddress:self.address];
        [copy setCurrencyCode:self.currencyCode];
        [copy setStopCheck:self.stopCheck];
        [copy setPiastresFactor:self.piastresFactor];
        [copy setBaseMinutesFactor:self.baseMinutesFactor];
        [copy setExperienceFactor:self.experienceFactor];
        [copy setInternalIdentifier:self.internalIdentifier];
        [copy setInternalSecret:self.internalSecret];
        [copy setLatitude:self.latitude];
        [copy setLongitude:self.longitude];
        [copy setTelephone:self.telephone];
        [copy setOpeningHours:self.openingHours];
        [copy setFacebook:self.facebook];
        [copy setInstagram:self.instagram];
        [copy setTwitter:self.twitter];
        [copy setVkontakte:self.vkontakte];
        [copy setWebsite:self.website];
        [copy setImageUrls:self.imageUrls];
        [copy setLastUpdated:self.lastUpdated];
        [copy setUnlimitedSubscriptionCost:self.unlimitedSubscriptionCost];
        [copy setWorkingSubscriptionCost:self.workingSubscriptionCost];
        [copy setWorkingSubscriptionStartTime:self.workingSubscriptionStartTime];
        [copy setWorkingSubscriptionEndTime:self.workingSubscriptionEndTime];
    }
    
    return copy;
}

- (void)validateVenue:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock {
    NSLog(@"validate venue");
    NSMutableString *displayMessage = [[NSMutableString alloc] initWithString:@""];
    
    [self validateStringField:self.name
             withErrorMessage:@"Please enter a name"
             toDisplayMessage:displayMessage];
    [self validateStringField:self.address
             withErrorMessage:@"Please enter the address"
             toDisplayMessage:displayMessage];
    [self validateStringField:self.openingHours
             withErrorMessage:@"Please enter opening hours"
             toDisplayMessage:displayMessage];
    
    NSUInteger imageCount = self.imageUrls.count;
    if (imageCount <= 0) {
        [self appendMessage:@"Please add at least one image." toDisplayMessage:displayMessage];
    }
    
    [self validateNumberField:self.unlimitedSubscriptionCost
             withErrorMessage:@"Please enter the unlimited subscription cost"
             toDisplayMessage:displayMessage];
    [self validateNumberField:self.workingSubscriptionCost
             withErrorMessage:@"Please enter the working subscription cost"
             toDisplayMessage:displayMessage];
    [self validateCost:self.piastresFactor
      withErrorMessage:NSLocalizedString(@"Minutes per Piastres must be greater than 0", @"Validatation message - Piastres Factor")
      toDisplayMessage:displayMessage];
    [self validateCost:self.baseMinutesFactor
      withErrorMessage:NSLocalizedString(@"Base minutes conversation rate must be greater than 0", @"Validatation message - Base Minute Factor")
      toDisplayMessage:displayMessage];
    [self validateCost:self.experienceFactor
      withErrorMessage:NSLocalizedString(@"Experience per minute must be greater than 0", @"Validatation message - Experience Factor")
      toDisplayMessage:displayMessage];
    
    
    if (displayMessage.length > 0) {
        validateResultBlock(NO, displayMessage);
    } else {
        validateResultBlock(YES, @"");
    }
    
}

- (void)validateNumberField:(NSNumber *)number
           withErrorMessage:(NSString *)message
           toDisplayMessage:(NSMutableString *)displayMessage {
    if (!number) {
        [self appendMessage:message toDisplayMessage:displayMessage];
    }
}

- (void)validateCost:(NSNumber *)cost
    withErrorMessage:(NSString *)message
    toDisplayMessage:(NSMutableString *)displayMessage {
    if (!cost || [cost compare:@0] != NSOrderedDescending) {
        [self appendMessage:message toDisplayMessage:displayMessage];
    }
}

- (void)validateStringField:(NSString *)text
           withErrorMessage:(NSString *)message
           toDisplayMessage:(NSMutableString *)displayMessage {
    NSString *trimmedString = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length == 0) {
        [self appendMessage:message toDisplayMessage:displayMessage];
    }
}

- (void)appendMessage:(NSString *)message
     toDisplayMessage:(NSMutableString *)displayMessage {
    if (displayMessage.length > 0) {
        [displayMessage appendString:@"\n"];
    }
    [displayMessage appendString:message];
    
}

@end
