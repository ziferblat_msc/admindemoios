//
//  LONews.h
//  ZiferblatAdmin
//
//  Created by Peter Su on 21/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LONews : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic) NSUInteger newsId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic) NSUInteger likeCount;
@property (nonatomic) NSUInteger commentCount;
@property (nonatomic, strong) NSDate *lastUpdated;

@property (nonatomic, strong) LOVenue *venue;

- (void)validateNews:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;

@end
