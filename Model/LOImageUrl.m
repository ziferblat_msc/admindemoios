//
//  LOImageUrl.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 26/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOImageUrl.h"

@implementation LOImageUrl

@synthesize image;
@synthesize url;
@synthesize thumbnailUrl;

@end
