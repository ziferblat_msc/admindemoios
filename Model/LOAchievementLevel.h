//
//  LOAchievementLevel.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 04/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LOAchievementLevel : LORESTModel

@property (nonatomic) NSInteger achievementLevelId;
@property (nonatomic) NSInteger sequenceIndex;
@property (nonatomic) NSInteger triggerFriends;
@property (nonatomic) NSInteger triggerTime;
@property (nonatomic) NSInteger triggerExperience;
@property (nonatomic) NSInteger triggerVisits;
@property (nonatomic) NSInteger triggerTasks;
@property (nonatomic) NSInteger rewardPiastres;
@property (nonatomic) NSInteger rewardTime;
@property (nonatomic) NSInteger rewardExperience;

@end
