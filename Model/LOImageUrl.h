//
//  LOImageUrl.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 26/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LOImageUrl : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *thumbnailUrl;

@end
