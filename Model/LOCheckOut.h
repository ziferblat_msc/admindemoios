//
//  LOCheckOut.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 01/09/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LOCheckOut : LORESTModel

@property (nonatomic) NSUInteger checkInId;
@property (nonatomic, strong) NSDate *checkInDate;
@property (nonatomic, strong) NSDate *checkOutDate;
@property (nonatomic) CGFloat totalTime;
@property (nonatomic) CGFloat cost;

@end
