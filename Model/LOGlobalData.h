//
//  LOGlobalData.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"
#import "LOEditableItem.h"

@interface LOGlobalData : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic, strong) NSNumber *minutesPerSubscription;
@property (nonatomic, strong) NSNumber *piastresPerDollar;
@property (nonatomic, strong) NSDate *lastUpdated;

- (void)validateGlobalData:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;

@end
