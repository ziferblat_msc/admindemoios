//
//  LOAchievementLevel.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 04/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOAchievementLevel.h"

@implementation LOAchievementLevel

@synthesize achievementLevelId;
@synthesize sequenceIndex;
@synthesize triggerExperience;
@synthesize triggerFriends;
@synthesize triggerTasks;
@synthesize triggerTime;
@synthesize triggerVisits;
@synthesize rewardExperience;
@synthesize rewardPiastres;
@synthesize rewardTime;

- (id)init {
    self = [super init];
    
    if(self != nil) {
        [super addMappingFromKey:@"id" toKey:@"achievementLevelId"];
    }
    
    return self;
}

@end
