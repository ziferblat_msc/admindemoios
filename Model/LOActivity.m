//
//  LOActivity.m
//  ZiferblatAdmin
//
//  Created by Peter Su on 23/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "LOActivity.h"
#import "LOVenue.h"
#import "LOMember.h"

@implementation LOActivity

- (instancetype)init {
    self = [super init];
    
    if(self != nil)
    {
        REST_MAP(@"id", @"activityId");
        REST_MAP(@"description", @"detail");
        REST_IGNORE_ENCODE(@"venue");
        REST_CLASS(@"attendees", [LOMember class]);
        REST_CLASS(@"completedList", [LOMember class]);
        REST_CLASS(@"queue", [LOQueuePosition class]);
    }
    
    return self;
}

- (id)valueForKey:(NSString *)key {
    if([key isEqualToString:@"date"]) {
        return @(self.date.timeIntervalSince1970*1000);
    } else if ([key isEqualToString:@"deadLine"]) {
        if (self.deadLine) {
            return @(self.deadLine.timeIntervalSince1970 * 1000);
        } else {
            return nil;
        }
    } else if ([key isEqualToString:@"dateCreated"]) {
        if (self.dateCreated) {
            return @(self.dateCreated.timeIntervalSince1970 * 1000);
        } else {
            return nil;
        }
    } else if ([key isEqualToString:@"lastUpdated"]) {
        if (self.lastUpdated) {
            return @(self.lastUpdated.timeIntervalSince1970 * 1000);
        } else {
            return nil;
        }
    } else {
        return [super valueForKey:key];
    }
}

#pragma mark - Custom date setters

- (void)setDate:(NSDate *)aValue {
    NSDate *aDate;
    
    if ([aValue isKindOfClass:[NSNumber class]]) {
        // Set from server
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        // Change within the app
        aDate = (NSDate *)aValue;
    }
    _date = aDate;
}

- (void)setLastUpdated:(NSObject *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        aDate = (NSDate *)aValue;
    }
    _lastUpdated = aDate;
}
- (void)setDeadLine:(NSDate *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        NSNumber *dateValue = (NSNumber *)aValue;
        if ([dateValue compare:@0] != NSOrderedSame) {
            aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
        } else {
            aDate = nil;
        }
    } else {
        aDate = (NSDate *)aValue;
    }
    _deadLine = aDate;
}
- (void)setDateCreated:(NSDate *)dateCreatedValue {
    NSDate *date;
    
    if ([dateCreatedValue isKindOfClass:[NSNumber class]]) {
        date = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)dateCreatedValue).doubleValue];
    } else {
        date = dateCreatedValue;
    }
    _dateCreated = date;
}

#pragma mark - Other

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[self class] new];
    if (copy) {
        [copy setActivityId:self.activityId];
        [copy setImageUrl:self.imageUrl];
        [copy setThumbnailUrl:self.thumbnailUrl];
        [copy setTitle:self.title];
        [copy setDetail:self.detail];
        [copy setDate:self.date];
        [copy setDeadLine:self.deadLine];
        [copy setLikeCount:self.likeCount];
        [copy setCommentCount:self.commentCount];
        [copy setAttendeeCount:self.attendeeCount];
        [copy setLastUpdated:self.lastUpdated];
        [copy setDateCreated:self.dateCreated];
        [copy setVenue:[self.venue copy]];
        [copy setPeopleNeeded:self.peopleNeeded];
        [copy setAttendees:self.attendees];
        [copy setCompleted:self.completed];
    }
    return copy;
}

- (void)validateActivity:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock {
    NSString *nameString = [self.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *imageURLString = [self.imageUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *detailString = [self.detail stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableString *message = [NSMutableString string];
    
    if(nameString.length == 0) {
        [self message:message appendWithError:NSLocalizedString(@"Please enter a name.", @"Alert")];
    }
    
    if(detailString.length == 0) {
        [self message:message appendWithError:NSLocalizedString(@"Please enter a description.", @"Alert")];
    }
    
    if(imageURLString.length == 0) {
        [self message:message appendWithError:NSLocalizedString(@"Please add an image.", @"Alert")];
    }
    
    if (!self.date) {
        [self message:message appendWithError:NSLocalizedString(@"Please enter a date.", @"Alert")];
    } else if ([[NSDate date] compare:self.date] != NSOrderedAscending) {
        [self message:message appendWithError:NSLocalizedString(@"End date cannot be in the past.", @"Alert")];
    }
    
    if (self.deadLine) {
        if ([[NSDate date] compare:self.deadLine] != NSOrderedAscending) {
            [self message:message appendWithError:NSLocalizedString(@"Event start cannot be in the past.", @"Alert")];
        }
        
        if ([self.deadLine compare:self.date] != NSOrderedAscending) {
            [self message:message appendWithError:NSLocalizedString(@"Event start has to be before date of activity.", @"Alert")];
        }
    }
    
    if (self.peopleNeeded < 1) {
        [self message:message appendWithError:NSLocalizedString(@"Number of people needed must be at least 1.", @"Alert")];
    }
    
    if (self.peopleNeeded < self.attendeeCount) {
        [self message:message appendWithError:NSLocalizedString(@"There are currently more people attending. Cannot reduce people needed.", @"Alert")];
    }
    
    if(message.length > 0) {
        validateResultBlock(NO, message);
    } else {
        validateResultBlock(YES, @"");
    }
}

- (void)message:(NSMutableString *)message appendWithError:(NSString *)errorMessage {
    if (message.length > 0) {
        [message appendString:@"\n"];
    }
    [message appendString:errorMessage];
}

@end
