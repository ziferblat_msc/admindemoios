//
//  LOImageDownloader.m
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 26/01/2016.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LOImageDownloader.h"
#import "LOImageCache.h"

@implementation LOImageDownloader

+ (void)downloadImageFromPath:(NSString*)path
        withCompletionHander:(LOImageDownloadCompletion)downloadComplete {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString *callingUrl = path;
        NSURL *imageURL = [NSURL URLWithString:callingUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([path isEqualToString:callingUrl]) {
                UIImage *profileImage = [UIImage imageWithData:imageData];
                [LOImageCache cacheImage:profileImage forName:callingUrl];
                // Call completion handler
                downloadComplete(profileImage);
            } else {
                NSLog(@"Image url mismatch.");
            }
        });
    });
}

@end
