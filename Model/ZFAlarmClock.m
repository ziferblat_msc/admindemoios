//
//  ZFAlarmClock.m
//  ZiferblatAdmin
//
//  Created by Nail Gabutdinov on 11.05.16.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "ZFAlarmClock.h"
#import "LOMember.h"

@implementation ZFAlarmClock

@synthesize id;
@synthesize name;
@synthesize imageUrl;
@synthesize members;
@synthesize venue;

- (instancetype)init
{
    self = [super init];
    
    if(self != nil)
    {
        REST_IGNORE_ENCODE(@"venue");
        //REST_CLASS(@"members", [LOMember class]);
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    if (copy)
    {
        [copy setId:self.id];
        [copy setName:self.name];
        [copy setImageUrl:self.imageUrl];
        [copy setMembers:self.members];
        [copy setVenue:self.venue];
    }
    return copy;
}

@end
