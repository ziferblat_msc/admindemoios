//
//  LOEvent.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOEvent.h"
#import "LOVenue.h"

@implementation LOEvent

@synthesize eventId;
@synthesize imageUrl;
@synthesize thumbnailUrl;
@synthesize title;
@synthesize detail;
@synthesize date;
@synthesize likeCount;
@synthesize commentCount;
@synthesize attendeeCount;
@synthesize lastUpdated;
@synthesize venue;

- (instancetype)init {
    self = [super init];
    
    if(self != nil) {
        REST_MAP(@"id", @"eventId");
        REST_MAP(@"description", @"detail");
        REST_IGNORE_ENCODE(@"venue");
    }
    
    return self;
}

- (id)valueForKey:(NSString *)key {
    if([key isEqualToString:@"date"]) {
        return @(date.timeIntervalSince1970*1000);
    } else {
        return [super valueForKey:key];
    }
}

- (void)setDate:(NSDate *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        aDate = (NSDate *)aValue;
    }
    
    date = aDate;
}

- (void)setLastUpdated:(NSObject *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        aDate = (NSDate *)aValue;
    }
    
    lastUpdated = aDate;
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    if (copy) {
        [copy setEventId:self.eventId];
        [copy setImageUrl:self.imageUrl];
        [copy setThumbnailUrl:self.thumbnailUrl];
        [copy setTitle:self.title];
        [copy setDetail:self.detail];
        [copy setDate:self.date];
        [copy setLikeCount:self.likeCount];
        [copy setCommentCount:self.commentCount];
        [copy setAttendeeCount:self.attendeeCount];
        [copy setLastUpdated:self.lastUpdated];
        [copy setVenue:[self.venue copy]];
    }
    return copy;
}

- (void)validateEvent:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock {
    NSString *nameString = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *imageURLString = [imageUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *detailString = [detail stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableString *message = [NSMutableString string];
    
    if(nameString.length == 0) {
        [message appendString:@"Please enter a name."];
    }
    
    if(detailString.length == 0) {
        if(message.length > 0) {
            [message appendString:@"\n"];
        }
        [message appendString:@"Please enter a description."];
    }
    
    if(imageURLString.length == 0) {
        if(message.length > 0) {
            [message appendString:@"\n"];
        }
        
        [message appendString:@"Please add an image."];
    }
    
    if(message.length > 0) {
        validateResultBlock(NO, message);
    } else {
        validateResultBlock(YES, @"");
    }
}

@end
