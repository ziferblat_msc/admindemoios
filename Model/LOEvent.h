//
//  LOEvent.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LOEvent : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic) NSUInteger eventId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic) NSUInteger likeCount;
@property (nonatomic) NSUInteger commentCount;
@property (nonatomic) NSUInteger attendeeCount;
@property (nonatomic, strong) NSDate *lastUpdated;

@property (nonatomic, strong) LOVenue *venue;

- (void)validateEvent:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;

@end
