//
//  LOAccountBalance.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 25/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"

@interface LOAccountBalance : LORESTModel

@property (nonatomic, strong) NSNumber *piastres;
@property (nonatomic, strong) NSNumber *baseMinutes;

@end
