//
//  LOGlobalData.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 18/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOGlobalData.h"

@implementation LOGlobalData

@synthesize minutesPerSubscription;
@synthesize piastresPerDollar;
@synthesize lastUpdated;

- (NSDate *)lastUpdated {
    return [NSDate date];
}

- (id)copyWithZone:(NSZone *)zone {
    id copy = [[[self class] alloc] init];
    if (copy) {
        [copy setMinutesPerSubscription:self.minutesPerSubscription];
        [copy setPiastresPerDollar:self.piastresPerDollar];
        [copy setLastUpdated:self.lastUpdated];
    }
    
    return copy;
}

- (void)validateGlobalData:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock {
    NSMutableString *message = [NSMutableString string];
    
    if (!piastresPerDollar || [piastresPerDollar compare:@0] != NSOrderedDescending) {
        [message appendString:@"Please enter a conversion rate."];
    }
    
    if (message.length > 0) {
        validateResultBlock(NO, message);
    } else {
        validateResultBlock(YES, @"");
    }
}

@end
