//
//  LOCheckOut.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 01/09/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOCheckOut.h"

@implementation LOCheckOut

@synthesize checkInId;
@synthesize checkInDate;
@synthesize checkOutDate;
@synthesize totalTime;
@synthesize cost;

- (instancetype)init {
    self = [super init];
    
    if(self != nil) {
        REST_MAP(@"id", @"checkInId");
    }
    
    return self;
}

- (id)valueForKey:(NSString *)key {
    if([key isEqualToString:@"checkInDate"]) {
        return @(checkInDate.timeIntervalSince1970*1000);
    } else if([key isEqualToString:@"checkOutDate"]) {
        return @(checkOutDate.timeIntervalSince1970*1000);
    } else {
        return [super valueForKey:key];
    }
}

- (void)setCheckInDate:(NSDate *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        aDate = (NSDate *)aValue;
    }
    
    checkInDate = aDate;
}

- (void)setCheckOutDate:(NSDate *)aValue {
    NSDate *aDate = nil;
    
    if([aValue isKindOfClass:[NSNumber class]]) {
        aDate = [NSDate dateWithTimeIntervalSince1970:((NSNumber *)aValue).longLongValue / 1000];
    } else {
        aDate = (NSDate *)aValue;
    }
    
    checkOutDate = aDate;
}

@end
