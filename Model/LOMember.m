//
//  LOMember.m
//  ZiferblatAdmin
//
//  Created by Simon Lee on 21/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOMember.h"

@implementation LOMember

@synthesize memberId;
@synthesize email;
@synthesize password;
@synthesize firstName;
@synthesize lastName;
@synthesize imageUrl;
@synthesize adminVenueIds;
@synthesize currentVenueId;
@synthesize favouriteVenueId;
@synthesize isCheckedIn;
@synthesize secretWord;
@synthesize role;
@synthesize gender;
@synthesize age;
@synthesize socialStatus;

- (id)init {
    self = [super init];
    
    if(self != nil) {
        [super addMappingFromKey:@"id" toKey:@"memberId"];
        [super addMappingFromKey:@"genderValue" toKey:@"gender"];
        [super addMappingFromKey:@"ageValue" toKey:@"age"];
        [super addMappingFromKey:@"socialStatusValue" toKey:@"socialStatus"];
        [super ignoreEncodeKey:@"fullName"];
        [super ignoreEncodeKey:@"isCheckedIn"];
    }
    
    return self;
}

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (bool)isCheckedIn {
    return ([[LOVenueService instance] isAssignedToVenue] && ([[LOVenueService instance] assignedVenueId] == currentVenueId));
}

#pragma mark - Helpers

#pragma mark MemberGenderType

+ (MemberGenderType)genderForTypeSelected:(NSString *)gender {
    if ([gender isEqualToString:kMemberGenderMale]) {
        return MemberGenderTypeMale;
    } else if ([gender isEqualToString:kMemberGenderFemale]) {
        return MemberGenderTypeFemale;
    }
    return MemberGenderTypeUnspecified;
}

+ (NSString *)stringForGender:(MemberGenderType)genderType {
    switch (genderType) {
        case MemberGenderTypeMale:{
            return kMemberGenderMale;
        }
        case MemberGenderTypeFemale: {
            return kMemberGenderFemale;
        }
        case MemberGenderTypeUnspecified:
            // Fall through
        default:
            return @"";
    }
}

#pragma mark MemberAgeRange

+ (MemberAgeRange)ageRangeForRangeSelected:(NSString *)range {
    if ([range isEqualToString:kMemberAgeRangeFourteenToEighteen]) {
        return MemberAgeRangeFourteenToEighteen;
    } else if ([range isEqualToString:kMemberAgeRangeNineteenToTwentyFive]) {
        return MemberAgeRangeNineteenToTwentyFive;
    } else if ([range isEqualToString:kMemberAgeRangeTwentySixToThirtyFive]) {
        return MemberAgeRangeTwentySixToThirtyFive;
    } else if ([range isEqualToString:kMemberAgeRangeThirtyFivePlus]) {
        return MemberAgeRangeThirtyFivePlus;
    }
    return MemberAgeRangeUnspecified;
}

+ (NSString *)stringForAgeRange:(MemberAgeRange)ageRange {
    switch (ageRange) {
        case MemberAgeRangeFourteenToEighteen:
            return kMemberAgeRangeFourteenToEighteen;
        case MemberAgeRangeNineteenToTwentyFive:
            return kMemberAgeRangeNineteenToTwentyFive;
        case MemberAgeRangeTwentySixToThirtyFive:
            return kMemberAgeRangeTwentySixToThirtyFive;
        case MemberAgeRangeThirtyFivePlus:
            return kMemberAgeRangeThirtyFivePlus;
        case MemberAgeRangeUnspecified:
            // Fall through
        default:
            return @"";
    }
}

#pragma mark MemberSocialStatusType

+ (MemberSocialStatusType)socialStatusForStatusSelected:(NSString *)status {
    if ([status isEqualToString:kMemberSocialSchoolStudent]) {
        return MemberSocialStatusTypeSchoolStudent;
    } else if ([status isEqualToString:kMemberSocialUniversityStudent]) {
        return MemberSocialStatusTypeUniversityStudent;
    } else if ([status isEqualToString:kMemberSocialCreativeJob]) {
        return MemberSocialStatusTypeCreativeJob;
    } else if ([status isEqualToString:kMemberSocialFreelancer]) {
        return MemberSocialStatusTypeFreelancer;
    } else if ([status isEqualToString:kMemberSocialUnemployed]) {
        return MemberSocialStatusTypeUnemployed;
    } else if ([status isEqualToString:kMemberSocialEmployed]) {
        return MemberSocialStatusTypeEmployed;
    } else if ([status isEqualToString:kMemberSocialEntrepreneur]) {
        return MemberSocialStatusTypeEntrepreneur;
    } else if ([status isEqualToString:kMemberSocialRetired]) {
        return MemberSocialStatusTypeRetired;
    }
    
    return MemberSocialStatusTypeUnspecified;
}

+ (NSString *)stringForSocialStatus:(MemberSocialStatusType)socialStatusType {
    switch (socialStatusType) {
        case MemberSocialStatusTypeSchoolStudent:
            return kMemberSocialSchoolStudent;
        case MemberSocialStatusTypeUniversityStudent:
            return kMemberSocialUniversityStudent;
        case MemberSocialStatusTypeCreativeJob:
            return kMemberSocialCreativeJob;
        case MemberSocialStatusTypeFreelancer:
            return kMemberSocialFreelancer;
        case MemberSocialStatusTypeUnemployed:
            return kMemberSocialUnemployed;
        case MemberSocialStatusTypeEmployed:
            return kMemberSocialEmployed;
        case MemberSocialStatusTypeEntrepreneur:
            return kMemberSocialEntrepreneur;
        case MemberSocialStatusTypeRetired:
            return kMemberSocialRetired;
        default:
            return @"";
    }
}

@end
