//
//  LOVenue.h
//  ZiferblatAdmin
//
//  Created by Simon Lee on 06/05/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LORESTModel.h"
#import "LOEditableItem.h"

@interface LOVenue : LORESTModel<LOEditableItem, NSCopying>

@property (nonatomic) NSUInteger venueId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSNumber *stopCheck;
@property (nonatomic, strong) NSNumber *piastresFactor; // Need
@property (nonatomic, strong) NSNumber *baseMinutesFactor;
@property (nonatomic, strong) NSNumber *experienceFactor;
@property (nonatomic, strong) NSString *internalIdentifier;
@property (nonatomic, strong) NSString *internalSecret;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *openingHours;
@property (nonatomic, strong) NSString *facebook;
@property (nonatomic, strong) NSString *instagram;
@property (nonatomic, strong) NSString *twitter;
@property (nonatomic, strong) NSString *vkontakte;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSArray *imageUrls;
@property (nonatomic, strong) NSDate *lastUpdated;
@property (nonatomic, strong) NSNumber *unlimitedSubscriptionCost;
@property (nonatomic, strong) NSNumber *workingSubscriptionCost;
@property (nonatomic, strong) NSDate *workingSubscriptionStartTime;
@property (nonatomic, strong) NSDate *workingSubscriptionEndTime;
@property (nonatomic, strong) NSNumber *dateCreated;

- (void)validateVenue:(void (^)(BOOL success, NSString *errorMessage))validateResultBlock;


@end
