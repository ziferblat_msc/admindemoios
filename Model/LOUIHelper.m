//
//  LOUIHelper.m
//  ZiferblatAdmin
//
//  Created by Boris Yurkevich on 26/01/2016.
//  Copyright © 2016 Locassa. All rights reserved.
//

#import "LOUIHelper.h"

@implementation LOUIHelper

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
